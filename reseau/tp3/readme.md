# TP3 : Progressons vers le réseau d'infrastructur

## I. (mini)Architecture réseau

### 🌞 Vous pouvez d'ores-et-déjà créer le routeur. Pour celui-ci, vous me prouverez que

il a bien une IP dans les 3 réseaux, l'IP que vous avez choisie comme IP de passerelle

```bash
[kaddate@localhost ~]$ ip a | grep "inet " | grep -v 127.0.0
    inet 192.168.138.128/24 brd 192.168.138.255 scope global dynamic noprefixroute ens33
    inet 10.3.1.126/25 brd 10.3.1.127 scope global noprefixroute ens36
    inet 10.3.1.206/28 brd 10.3.1.207 scope global noprefixroute ens37
    inet 10.3.1.190/26 brd 10.3.1.191 scope global noprefixroute ens38
```

il a un accès internet

```bash
[kaddate@localhost ~]$ ping 8.8.8.8 -c2 | grep "packet los"
2 packets transmitted, 2 received, 0% packet loss, time 1001ms
```

il a de la résolution de noms

```bash
[kaddate@localhost ~]$ dig | grep ";; SERVER"
;; SERVER: 192.168.138.2#53(192.168.138.2)
```

il porte le nom router.tp3*

```bash
[kaddate@localhost ~]$ cat /etc/hostname
router.tp3
```

n'oubliez pas d'activer le routage sur la machine

```bash
[kaddate@localhost ~]$ sudo firewall-cmd --add-masquerade --zone=public
success
[kaddate@localhost ~]$ sudo firewall-cmd --add-masquerade --zone=public --permanent
success
```

## II. Services d'infra

## 1. Serveur DHCP

### 🌞 Mettre en place une machine qui fera office de serveur DHCP dans le réseau client1. Elle devra

porter le nom dhcp.client1.tp3

```bash
[kaddate@localhost ~]$ cat /etc/hostname 
dhcp.client1.tp3
```

donner une IP aux machines clients qui le demande

leur donner l'adresse de leur passerelle

leur donner l'adresse d'un DNS utilisable

```bash
[kaddate@dhcp dhcp]$ cat dhcpd.conf | egrep "option|range"
  range 10.3.1.129 10.3.1.180;
  option routers 10.3.1.190;
  option subnet-mask 255.255.255.192;
  option domain-name-servers 1.1.1.1;
```

### 🌞 Mettre en place un client dans le réseau client1

de son p'tit nom marcel.client1.tp3

```bash
[kaddate@dhcp ~]$ cat /etc/hostname
dhcp.client1.tp3
```

la machine récupérera une IP dynamiquement grâce au serveur DHCP

ainsi que sa passerelle et une adresse d'un DNS utilisable

```bash
[kaddate@localhost network-scripts]$ sudo dhclient
[kaddate@localhost network-scripts]$ ip a s dev ens36 | grep "inet "
    inet 10.3.1.131/26 brd 10.3.1.191 scope global dynamic ens36

[kaddate@localhost network-scripts]$ ip r
default via 10.3.3.62 dev ens36 
10.3.3.0/26 dev ens36 proto kernel scope link src 10.3.3.2 

[kaddate@localhost network-scripts]$ dig | grep ";; SERVER"
;; SERVER: 1.1.1.1#53(1.1.1.1)
```

### 🌞 Depuis marcel.client1.tp3

prouver qu'il a un accès internet + résolution de noms, avec des infos récupérées par votre DHCP

```bash
[kaddate@localhost network-scripts]$ ping ynov.com -c2 | grep "packet loss"
2 packets transmitted, 2 received, 0% packet loss, time 1000ms
```

à l'aide de la commande traceroute, prouver que marcel.client1.tp3 passe par router.tp3 pour sortir de son réseau

```bash
[kaddate@localhost network-scripts]$ traceroute ynov.com | grep _gateway
 1  _gateway (10.3.1.190)  1.544 ms  1.446 ms  1.362 ms
```

## 2. Serveur DNS

## B. SETUP copain

### 🌞 Mettre en place une machine qui fera office de serveur DNS

dans le réseau server1

de son p'tit nom dns1.server1.tp3

```bash
[kaddate@localhost ~]$ cat /etc/hostname 
dns1.server1.tp3
```

il faudra lui ajouter un serveur DNS public connu, afin qu'il soit capable de résoudre des noms publics comme google.com

```bash
[kaddate@localhost ~]$ cat /etc/resolv.conf | grep 1.1
nameserver 1.1.1.1
```

comme pour le DHCP, on part sur "rocky linux dns server" on Google pour les détails de l'install

le paquet que vous allez installer devrait s'appeler bind : c'est le nom du serveur DNS le plus utilisé au monde

```bash
[kaddate@dns1 ~]$ sudo yum install bind bind-utils -y | grep "Terminé !"
Terminé !
```

il y aura plusieurs fichiers de conf :

un fichier de conf principal
des fichiers de zone "forward"

permet d'indiquer une correspondance nom -> IP

des fichiers de zone "reverse"

permet d'indiquer une correspondance IP -> nom

on ne met PAS les clients dans les fichiers de zone car leurs adresses IP peuvent changer (ils les récupèrent à l'aide du DHCP)

```bash
[kaddate@dns1 etc]$ sudo cat /etc/named.conf
//
// named.conf
[...]
zone "server1.tp3" IN {
        type master;
        file "/etc/server1.tp3.forward";
        allow-update {10.3.1.189;};
};

zone "server2.tp3" IN {
        type master;
        file "/etc/server2.tp3.forward";
        allow-update {10.3.1.189;};
};
[...]
```

```bash
# fichier de conf de la premiere zone forward
[kaddate@dns1 etc]$ cat forward.server1.tp3 
$TTL 86400
@   IN  SOA dns1.server1.tp3. admin.server1.tp3. (
        2021062301   ; serial
        3600         ; refresh
        1800         ; retry
        604800       ; expire
        86400 )      ; minimum TTL
;
; define nameservers
    IN  NS  dns1.server1.tp3.
;
; DNS Server IP addresses and hostnames
dns1 IN  A   10.3.1.2
;
;client records
router IN  A   10.3.1.126
```

### 🌞 Tester le DNS depuis marcel.client1.tp3

définissez manuellement l'utilisation de votre serveur DNS

```bash
[kaddate@marcel ~]$ cat /etc/resolv.conf | grep 10.3
nameserver 10.3.1.2
```

essayez une résolution de nom avec dig

une résolution de nom classique

on teste la zone forward

```bash
[kaddate@marcel ~]$ dig router.server1.tp3 | sed -n "14,15p"
;; ANSWER SECTION:
router.server1.tp3.	86400	IN	A	10.3.1.126
```

prouvez que c'est bien votre serveur DNS qui répond pour chaque dig

```bash
[kaddate@marcel ~]$ dig router.server1.tp3 | grep ";; SERVER"
;; SERVER: 10.3.1.2#53(10.3.1.2)
```

### 🌞 Configurez l'utilisation du serveur DNS sur TOUS vos noeuds

les serveurs, on le fait à la main

```bash
# exemple pour le dhcp
[kaddate@dhcp ~]$ cat /etc/resolv.conf | grep 10.3
nameserver 10.3.1.2
```

les clients, c'est fait via DHCP

```bash
[kaddate@dhcp ~]$ cat /etc/dhcp/dhcpd.conf | grep domain
  option domain-name-servers 1.1.1.1, 10.3.1.2;
```

## 3. Get deeper

## A. DNS forwarder

### 🌞 Affiner la configuration du DNS

faites en sorte que votre DNS soit désormais aussi un forwarder DNS

c'est à dire que s'il ne connaît pas un nom, il ira poser la question à quelqu'un d'autre

```bash
[kaddate@dns1 ~]$ sudo cat /etc/named.conf | grep "recursion yes"
	recursion yes;
```

### 🌞 Test !

vérifier depuis marcel.client1.tp3 que vous pouvez résoudre des noms publics comme google.com en utilisant votre propre serveur DNS (commande dig)

pour que ça fonctionne, il faut que dns1.server1.tp3 soit lui-même capable de résoudre des noms, avec 1.1.1.1 par exemple

```bash
[kaddate@marcel ~]$ dig google.com | grep ";; SERVER"
;; SERVER: 10.3.1.2#53(10.3.1.2)
[kaddate@marcel ~]$ dig google.com | egrep "^\google.com."
google.com.		169	IN	A	142.250.179.110
```

## B. On revient sur la conf du DHCP

### 🌞 Affiner la configuration du DHCP

faites en sorte que votre DHCP donne désormais l'adresse de votre serveur DNS aux clients

```bash
# conf dhcp
[kaddate@dhcp ~]$ cat /etc/dhcp/dhcpd.conf | grep domain
  option domain-name-servers 10.3.1.2;
```

créer un nouveau client johnny.client1.tp3 qui récupère son IP, et toutes les nouvelles infos, en DHCP

```bash
[kaddate@johnny ~]$ ip a s ens33 | grep dynamic
    inet 10.3.1.132/26 brd 10.3.1.191 scope global dynamic noprefixroute ens33
[kaddate@johnny ~]$ dig | grep ";; SERVER"
;; SERVER: 10.3.1.2#53(10.3.1.2)
```

## III. Services métier

## 1. Serveur Web

### 🌞 Setup d'une nouvelle machine, qui sera un serveur Web, une belle appli pour nos clients

vous utiliserez le serveur web que vous voudrez, le but c'est d'avoir un serveur web fast, pas d'y passer 1000 ans :)

dans tous les cas, n'oubliez pas d'ouvrir le port associé dans le firewall pour que le serveur web soit joignable

```bash
# j'ai installé apache
   35  sudo dnf install httpd
   37  sudo systemctl enable httpd
   38  sudo systemctl start httpd
   39  sudo systemctl status httpd
   45  sudo firewall-cmd --add-port=80/tcp --permanent
   46  sudo firewall-cmd --reload
   48  history
```

### 🌞 Test test test et re-test

testez que votre serveur web est accessible depuis marcel.client1.tp3

```bash
[kaddate@marcel ~]$ curl http://10.3.1.193:80 | wc -l
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  7621  100  7621    0     0  1240k      0 --:--:-- --:--:-- --:--:-- 1488k
308
```

## 2. Partage de fichiers

## B. Le setup wola

### 🌞 Setup d'une nouvelle machine, qui sera un serveur NFS

vous partagerez un dossier créé à cet effet : /srv/nfs_share/

```bash
[kaddate@nfs1 ~]$ cat /etc/idmapd.conf | grep server2
Domain = server2.tp3
[kaddate@nfs1 ~]$ cat /etc/exports
/srv/nfs_share 10.3.1.192/28(rw,no_root_squash)
```

### 🌞 Configuration du client NFS

effectuez de la configuration sur web1.server2.tp3 pour qu'il accède au partage NFS

le partage NFS devra être monté dans /srv/nfs/

```bash
[kaddate@web1 ~]$ echo "nfs1.server2.tp3:/srv/nfs_share /srv/nfs               nfs     defaults        0 0" | sudo tee -a /etc/fstab
nfs1.server2.tp3:/srv/nfs_share /srv/nfs               nfs     defaults        0 0
```

### 🌞 TEEEEST

tester que vous pouvez lire et écrire dans le dossier /srv/nfs depuis web1.server2.tp3

vous devriez voir les modifications du côté de  nfs1.server2.tp3 dans le dossier /srv/nfs_share/

```bash
# avant création d'un fichier (coté serveur).
[kaddate@nfs1 ~]$ sudo ls /srv/nfs_share/ -la && date
total 0
drwxr-xr-x. 2 root root  6  8 oct.  18:11 .
drwxr-xr-x. 3 root root 23  8 oct.  18:11 ..
ven. oct.  8 18:23:35 CEST 2021
```

```bash
# création d'un fichier (coté client).
[kaddate@web1 ~]$ sudo touch /srv/nfs/test && date
ven. oct.  8 19:05:21 CEST 2021
```

```bash
# après création du fichier (coté serveur).
[kaddate@nfs1 ~]$ ls -al /srv/nfs_share/ && date
total 0
drwxr-xr-x. 2 root root 18  8 oct.  19:05 .
drwxr-xr-x. 3 root root 23  8 oct.  18:11 ..
-rw-r--r--. 1 root root  0  8 oct.  19:05 test
ven. oct.  8 19:06:35 CEST 2021
```

## IV. Un peu de théorie : TCP et UDP

### 🌞 Déterminer, pour chacun de ces protocoles, s'ils sont encapsulés dans du TCP ou de l'UDP

tous les fichiers sont présent dans le dossier.

SSH = TCP

HTTP = TCP

DNS = UDP

NFS = TCP

### 🌞 Capturez et mettez en évidence un 3-way handshake

ci-dessous ma capture tcpdump de ma requête http dans laquelle j'ai extrait le 3-WAY handshake.

![si cette image ne s'affiche pas, le fichier est présent dans le meme dossier que le markdown au nom de "network-manager.png"](./3-way-handshake.png?raw=true)

## V. El final

### 🌞 Bah j'veux un schéma

le schéma

![si cette image ne s'affiche pas, le fichier est présent dans le meme dossier que le markdown au nom de "schéma_réseau_tp3.drawio.png"](./schéma_réseau_tp3.drawio.png?raw=true)

le 🗃️ tableau des réseaux 🗃️

| Nom du réseau | Adresse du réseau | Masque        | Nombre de clients possibles | Adresse passerelle | [Adresse broadcast](../../cours/lexique/README.md#adresse-de-diffusion-ou-broadcast-address) |
|---------------|-------------------|---------------|-----------------------------|--------------------|----------------------------------------------------------------------------------------------|
| `server1`     | `10.3.1.0`        | `255.255.255.128` | 126    | `10.3.1.126`       | `10.3.1.127`         |
| `client1`     | `10.3.1.128`        | `255.255.255.192` | 62     | `10.3.1.190`        | `10.3.1.191`          |
| `server2`     | `10.3.1.192`        | `255.255.255.240` | 14     | `10.3.1.206`        | `10.3.1.207`          |

le 🗃️ tableau d'adressage 🗃️

| Nom machine  | Adresse IP `client1` | Adresse IP `server1` | Adresse IP `server2` | Adresse de passerelle |
|--------------|----------------------|----------------------|----------------------|-----------------------|
| `router.tp3` | `10.3.1.190/26`       | `10.3.1.126/25`      | `10.3.1.206/28`       | Carte NAT           |
| `marcel.client1.tp3` | `dynamique`     | x               | x                      | 10.3.1.190/26          |
| `dhcp.client1.tp3` | `10.3.1.189/26`       | x                | x                 |  10.3.1.190/26        |
| `dns.server1.tp3` | x     | `10.3.1.2/25`               | x                      | 10.3.1.126/25          |
| `johnny.client1.tp3` | `dynamique`     | x               | x                      | 10.3.1.190/26          |
| `web1.server2.tp3` | x   | x               | `10.3.1.193/28`                      | 10.3.1.206/28          |
| `nfs1.server2.tp3` | x   | x               | `10.3.1.194/28`                      | 10.3.1.206/28          |

### 🌞 Et j'veux des fichiers aussi, tous les fichiers de conf du DNS

[forward.server1.tp3](forward.server1.tp3)

[forward.server2.tp3](forward.server2.tp3)

[named.conf](named.conf)
