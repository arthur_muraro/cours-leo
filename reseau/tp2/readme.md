# TP2 : On va router des trucs

## I. ARP

## 1. Echange ARP

### 🌞Générer des requêtes ARP

effectuer un ping d'une machine à l'autre

```bash
[kaddate@node1 ~]$ ping 10.2.1.12 -c2
PING 10.2.1.12 (10.2.1.12) 56(84) bytes of data.
64 bytes from 10.2.1.12: icmp_seq=1 ttl=64 time=0.381 ms
64 bytes from 10.2.1.12: icmp_seq=2 ttl=64 time=0.937 ms

--- 10.2.1.12 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1019ms
rtt min/avg/max/mdev = 0.381/0.659/0.937/0.278 ms
```

observer les tables ARP des deux machines
repérer l'adresse MAC de node1 dans la table ARP de node2 et vice-versa

```bash
[kaddate@node1 ~]$ arp -a | grep 10.2 | awk -F' ' '{print $4}'
00:0c:29:63:c6:e6
```

```bash
[kaddate@node2 ~]$ arp -a | grep 10.2 | awk -F' ' '{print $4}'
00:0c:29:50:49:71
```

prouvez que l'info est correcte (que l'adresse MAC que vous voyez dans la table est bien celle de la machine correspondante)

```bash
[kaddate@node1 ~]$ ip addr sh ens36 | grep link/ether | awk -F' ' '{print $2}'
00:0c:29:50:49:71
```

```bash
[kaddate@node2 ~]$ ip addr sh ens36 | grep link/ether | awk -F' ' '{print $2}'
00:0c:29:63:c6:e6
```

## 2. Analyse de trames

### 🌞Analyse de trames

utilisez la commande tcpdump pour réaliser une capture de trame

```bash
sudo tcpdump -i ens36 -c 25 -w premiertcpdump.pcap &
```

videz vos tables ARP, sur les deux machines, puis effectuez un ping

```bash
# commande pour vider mes tables arp
sudo ip -s -s neigh flush all
```

stoppez la capture, et exportez-la sur votre hôte pour visualiser avec Wireshark
mettez en évidence les trames ARP

```bash
# commande pour exporter le fichier
scp kaddate@192.168.138.129:/home/kaddate/premiertcpdump.pcap premiertcpdump.pcap
```

![si cette image ne s'affiche pas, le fichier est présent dans le meme dossier que le markdown au nom de "tcpdump_arp.png"](./tcpdump_arp.png?raw=true)

**écrivez, dans l'ordre, les échanges ARP qui ont eu lieu, puis le ping et le pong, je veux TOUTES les trames**

| ordre | type trame  | IP source | MAC source                | IP destination | MAC destination            |
|-------|-------------|-----------|---------------------------|----------------|----------------------------|
| 1     | Ping        | 10.2.1.11 | VMware_50:49:71           | 10.2.1.12      | VMware_63:c6:e6            |
| 2     | Pong        | 10.2.1.12 | VMware_63:c6:e6           | 10.2.1.11      | VMware_50:49:71            |
| 3     | Ping        | 10.2.1.11 | VMware_50:49:71           | 10.2.1.12      | VMware_63:c6:e6            |
| 4     | Pong        | 10.2.1.12 | VMware_63:c6:e6           | 10.2.1.11      | VMware_50:49:71            |
| 5     | Ping        | 10.2.1.11 | VMware_50:49:71           | 10.2.1.12      | VMware_63:c6:e6            |
| 6     | Pong        | 10.2.1.12 | VMware_63:c6:e6           | 10.2.1.11      | VMware_50:49:71            |
| 7     | Ping        | 10.2.1.11 | VMware_50:49:71           | 10.2.1.12      | VMware_63:c6:e6            |
| 8     | Pong        | 10.2.1.12 | VMware_63:c6:e6           | 10.2.1.11      | VMware_50:49:71            |
| 9     | Requête ARP | x         | `node2` `VMware_63:c6:e6` | x              | Broadcast `FF:FF:FF:FF:FF` |
| 10    | Réponse ARP | x         | `node1` `VMware_50:49:71` | x              | `node2` `VMware_63:c6:e6`  |
| 11    | Requête ARP | x         | `node1` `VMware_50:49:71` | x              | Broadcast `FF:FF:FF:FF:FF` |
| 12    | Réponse ARP | x         | `node2` `VMware_63:c6:e6` | x              | `node1` `VMware_50:49:71`  |

## II. Routage

## 1. Mise en place du routage

### 🌞 Activer le routage sur le noeud router.2.tp2

```bash
# On repère la zone utilisée par firewalld, généralement 'public' si vous n'avez pas fait de conf spécifique
[kaddate@router ~]$ sudo firewall-cmd --get-active-zone
public
  interfaces: ens33 ens36 ens37

# Activation du masquerading
[kaddate@router ~]$ sudo firewall-cmd --add-masquerade --zone=public
success
[kaddate@router ~]$ sudo firewall-cmd --add-masquerade --zone=public --permanent
success
```

### 🌞 Ajouter les routes statiques nécessaires pour que node1.net1.tp2 et marcel.net2.tp2 puissent se ping

```bash
# route du node1
[kaddate@node1 ~]$ sudo ip r a 10.2.2.0/24 via 10.2.1.11 dev ens36
[kaddate@node1 ~]$ ip r
10.2.1.0/24 dev ens36 proto kernel scope link src 10.2.1.12 metric 100 
10.2.2.0/24 via 10.2.1.11 dev ens36

# route de marcel
[kaddate@marcel ~]$ sudo ip r a 10.2.1.0/24 via 10.2.2.11 dev ens37
[kaddate@marcel ~]$ ip r
10.2.1.0/24 via 10.2.2.11 dev ens37 
10.2.2.0/24 dev ens37 proto kernel scope link src 10.2.2.12 metric 100 
```

**une fois les routes en place, vérifiez avec un ping que les deux machines peuvent se joindre**

```bash
[kaddate@node1 ~]$ ping 10.2.2.12 -c2 | grep "packet loss"
2 packets transmitted, 2 received, 0% packet loss, time 1024ms
```

## 2. Analyse de trames

### 🌞 Analyse des échanges ARP

videz les tables ARP des trois noeuds

```bash
# commande pour vider les tables arp
sudo ip -s -s neigh flush all
```

**effectuez un ping de node1.net1.tp2 vers marcel.net2.tp2 regardez les tables ARP des trois noeuds. Essayez de déduire un peu les échanges ARP qui ont eu lieu**

le router a dans sa table les deux VM, tandis que les autres n'ont que le router. étant donné qu'il n'y a que le router qui communique directemment avec les deux machines, et que les broadcast ne sont pas géré par les routers il est logique que les VM n'aient que l'adresse du router.

**écrivez, dans l'ordre, les échanges ARP qui ont eu lieu, puis le ping et le pong, je veux TOUTES les trames**

```bash
ping de node1 vers marcel
pour avoir toutes les trames, dans l'ordre il suffit de regarder les trames du router qui aura toutes les requetes arp et tout les paquets icmp.
```

| ordre | type trame  | IP source | MAC source                | IP destination | MAC destination            |
|-------|-------------|-----------|---------------------------|----------------|----------------------------|
| 1     | Requête ARP | x         | 00:0c:29:a8:2b:bb         | x              | Broadcast `FF:FF:FF:FF:FF` |
| 2     | Réponse ARP | x         | 00:0c:29:87:fd:2e         | x              | 00:0c:29:a8:2b:bb          |
| 3     | Ping        | 10.2.1.12 | 00:0c:29:a8:2b:bb         | 10.2.2.12      | 00:0c:29:7b:7e:e1          |
| 4     | Requête ARP | x         | 00:0c:29:87:fd:38         | x              | Broadcast `FF:FF:FF:FF:FF` |
| 5     | Réponse ARP | x         | 00:0c:29:7b:7e:e1         | x              | 00:0c:29:87:fd:38          |
| 6     | Ping        | 10.2.2.11 | 00:0c:29:87:fd:38         | 10.2.2.12      | 00:0c:29:7b:7e:e1          |
| 7     | Pong        | 10.2.2.12 | 00:0c:29:7b:7e:e1         | 10.2.2.11      | 00:0c:29:87:fd:38          |
| 8     | Pong        | 10.2.2.12 | 00:0c:29:7b:7e:e1         | 10.2.1.12      | 00:0c:29:a8:2b:bb          |
| 9     | Ping        | 10.2.1.12 | 00:0c:29:a8:2b:bb         | 10.2.2.12      | 00:0c:29:7b:7e:e1          |
| 10    | Ping        | 10.2.2.11 | 00:0c:29:87:fd:38         | 10.2.2.12      | 00:0c:29:7b:7e:e1          |
| 11    | Pong        | 10.2.2.12 | 00:0c:29:7b:7e:e1         | 10.2.2.11      | 00:0c:29:87:fd:38          |
| 12    | Pong        | 10.2.2.12 | 00:0c:29:7b:7e:e1         | 10.2.1.12      | 00:0c:29:a8:2b:bb          |
| 13    | Ping        | 10.2.1.12 | 00:0c:29:a8:2b:bb         | 10.2.2.12      | 00:0c:29:7b:7e:e1          |
| 14    | Ping        | 10.2.2.11 | 00:0c:29:87:fd:38         | 10.2.2.12      | 00:0c:29:7b:7e:e1          |
| 15    | Pong        | 10.2.2.12 | 00:0c:29:7b:7e:e1         | 10.2.2.11      | 00:0c:29:87:fd:38          |
| 16    | Pong        | 10.2.2.12 | 00:0c:29:7b:7e:e1         | 10.2.1.12      | 00:0c:29:a8:2b:bb          |
| 17    | Ping        | 10.2.1.12 | 00:0c:29:a8:2b:bb         | 10.2.2.12      | 00:0c:29:7b:7e:e1          |
| 18    | Ping        | 10.2.2.11 | 00:0c:29:87:fd:38         | 10.2.2.12      | 00:0c:29:7b:7e:e1          |
| 19    | Pong        | 10.2.2.12 | 00:0c:29:7b:7e:e1         | 10.2.2.11      | 00:0c:29:87:fd:38          |
| 20    | Pong        | 10.2.2.12 | 00:0c:29:7b:7e:e1         | 10.2.1.12      | 00:0c:29:a8:2b:bb          |
| 21    | Requête ARP | x         | 00:0c:29:7b:7e:e1         | x              | Broadcast `FF:FF:FF:FF:FF` |
| 22    | Réponse ARP | x         | 00:0c:29:87:fd:38         | x              | 00:0c:29:7b:7e:e1          |
| 23    | Requête ARP | x         | 00:0c:29:87:fd:2e         | x              | Broadcast `FF:FF:FF:FF:FF` |
| 24    | Réponse ARP | x         | 00:0c:29:a8:2b:bb         | x              | 00:0c:29:87:fd:2e          |

## 3. Accès internet

### 🌞 Donnez un accès internet à vos machines

ajoutez une route par défaut à node1.net1.tp2 et marcel.net2.tp2

```bash
[kaddate@node1 ~]$ sudo ip r a 0.0.0.0/1 via 10.2.1.11 dev ens36

[kaddate@marcel ~]$ sudo ip r a 0.0.0.0/1 via 10.2.2.11 dev ens37
```

vérifiez que vous avez accès internet avec un ping

```bash
[kaddate@node1 ~]$ ping 8.8.8.8 -c2 | grep "packet loss"
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
```

donnez leur aussi l'adresse d'un serveur DNS qu'ils peuvent utiliser

```bash
[kaddate@node1 ~]$ sudo sed -i '2 i nameserver 1.1.1.1' /etc/resolv.conf

[kaddate@marcel ~]$ sudo sed -i '2 i nameserver 1.1.1.1' /etc/resolv.conf
```

vérifiez que vous avez une résolution de noms qui fonctionne avec dig

```bash
[kaddate@node1 ~]$ dig | grep ";; SERVER"
;; SERVER: 1.1.1.1#53(1.1.1.1)
```

puis avec un ping vers un nom de domaine

```bash
[kaddate@node1 ~]$ ping ynov.com -c2 | grep "packet loss"
2 packets transmitted, 2 received, 0% packet loss, time 1000ms
```

### 🌞 Analyse de trames

effectuez un ping 8.8.8.8 depuis node1.net1.tp2

capturez le ping depuis node1.net1.tp2 avec tcpdump

analysez un ping aller et le retour qui correspond et mettez dans un tableau :

| ordre | type trame  | IP source       | MAC source                | IP destination | MAC destination            |
|-------|-------------|-----------------|---------------------------|----------------|----------------------------|
| 1     | Ping        | 10.2.1.12       | 00:0c:29:a8:2b:bb         | 8.8.8.8        | 00:0c:29:87:fd:2e          |
| 2     | Pong        | 8.8.8.8         | 00:0c:29:87:fd:2e         | 10.2.1.12      | 00:0c:29:a8:2b:bb          |
| 3     | Ping        | 10.2.1.12       | 00:0c:29:a8:2b:bb         | 8.8.8.8        | 00:0c:29:87:fd:2e          |
| 4     | Pong        | 8.8.8.8         | 00:0c:29:87:fd:2e         | 10.2.1.12      | 00:0c:29:a8:2b:bb          |
| 5     | Ping        | 10.2.1.12       | 00:0c:29:a8:2b:bb         | 8.8.8.8        | 00:0c:29:87:fd:2e          |
| 6     | Pong        | 8.8.8.8         | 00:0c:29:87:fd:2e         | 10.2.1.12      | 00:0c:29:a8:2b:bb          |
| 7     | Requête ARP | x               | 00:0c:29:a8:2b:bb         | x              | 00:0c:29:87:fd:2e          |
| 8     | Réponse ARP | x               | 00:0c:29:87:fd:2e         | x              | 00:0c:29:a8:2b:bb          |
| 9     | Requête ARP | x               | 00:0c:29:a8:2b:bb         | x              | 00:0c:29:87:fd:2e          |
| 10    | Réponse ARP | x               | 00:0c:29:87:fd:2e         | x              | 00:0c:29:a8:2b:bb          |

## III. DHCP

## 1. Mise en place du serveur DHCP

### 🌞 Sur la machine node1.net1.tp2, vous installerez et configurerez un serveur DHCP (go Google "rocky linux dhcp server")

installation du serveur sur node1.net1.tp2

```bash
# commande d'installation du dhcp
[kaddate@node1 ~]$ sudo dnf install dhcp-server
```

créer une machine node2.net1.tp2

faites lui récupérer une IP en DHCP à l'aide de votre serveur

```bash
[kaddate@node2 ~]$ ip a s dev ens36 | grep dynamic
    inet 10.2.1.50/24 brd 10.2.1.255 scope global dynamic noprefixroute ens36
```

### 🌞 Améliorer la configuration du DHCP

ajoutez de la configuration à votre DHCP pour qu'il donne aux clients, en plus de leur IP :

une route par défaut

un serveur DNS à utiliser

```bash
# fichier de conf dhcp

#
# DHCP Server Configuration file.
#   see /usr/share/doc/dhcp-server/dhcpd.conf.example
#   see dhcpd.conf(5) man page
#
default-lease-time 900;
max-lease-time 10800;
ddns-update-style none;
authoritative;
subnet 10.2.1.0 netmask 255.255.255.0 {
  range 10.2.1.50 10.2.1.100;
  option routers 10.2.1.11;
  option subnet-mask 255.255.255.0;
  option domain-name-servers 1.1.1.1;
}
```

récupérez de nouveau une IP en DHCP sur node2.net1.tp2 pour tester :

vérifier avec une commande qu'il a récupéré son IP

```bash
[kaddate@node2 ~]$ ip a s dev ens36 | grep "inet "
    inet 10.2.1.50/24 brd 10.2.1.255 scope global dynamic noprefixroute ens36
```

vérifier qu'il peut ping sa passerelle

```bash
[kaddate@node2 ~]$ ping -c2 10.2.1.11 | grep "packet loss"
2 packets transmitted, 2 received, 0% packet loss, time 1008ms
```

il doit avoir une route par défaut

vérifier la présence de la route avec une commande

```bash
[kaddate@node2 ~]$ ip r | grep default
default via 10.2.1.11 dev ens36 proto dhcp metric 100
```

vérifier que la route fonctionne avec un ping vers une IP

```bash
[kaddate@node2 ~]$ ping 8.8.8.8 -c2 | grep "packet loss"
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
```

il doit connaître l'adresse d'un serveur DNS pour avoir de la résolution de noms

vérifier avec la commande dig que ça fonctionne

vérifier un ping vers un nom de domaine

```bash
[kaddate@node2 ~]$ dig | grep ";; SERVER"
;; SERVER: 1.1.1.1#53(1.1.1.1)

[kaddate@node2 ~]$ ping ynov.com -c2 | grep "packet loss"
2 packets transmitted, 2 received, 0% packet loss, time 1001ms
```

## 2. Analyse de trames

### 🌞 Analyse de trames

videz les tables ARP des machines concernées

```bash
[kaddate@node2 ~]$ sudo ip -s -s neigh flush all

[kaddate@node1 ~]$ sudo ip -s -s neigh flush all
```

lancer une capture à l'aide de tcpdump afin de capturer un échange DHCP

```bash
# capture sur la machine node2 (le client)
[kaddate@node2 ~]$ sudo tcpdump -i ens36 -w capturedora.pcap port not 22
```

répéter une opération de renouvellement de bail DHCP, et demander une nouvelle IP afin de générer un échange DHCP

```bash
sudo dhclient -r
sudo dhclient
```

mettez en évidence l'échange DHCP DORA (Disover, Offer, Request, Acknowledge)
écrivez, dans l'ordre, les échanges ARP + DHCP qui ont lieu, je veux TOUTES les trames utiles pour l'échange

| ordre | type trame    | IP source | MAC source                | IP destination | MAC destination            |
|-------|-------------  |-----------|---------------------------|----------------|----------------------------|
| 1     | Requête ARP   | x         | 00:0c:29:90:1b:84         | x              | Broadcast `FF:FF:FF:FF:FF` |
| 2     | Réponse ARP   | x         | 00:0c:29:a8:2b:bb         | x              | 00:0c:29:90:1b:84          |
| 3     | DHCP release  | 10.2.1.50 | 00:0c:29:90:1b:84         | 10.2.1.12      | 00:0c:29:a8:2b:bb          |
| 4     | Ping          | 10.2.2.11 | 00:0c:29:a8:2b:bb         | 10.2.2.12      | 00:0c:29:90:1b:84          |
| 5     | DHCP discover | 0.0.0.0   | 00:0c:29:90:1b:84         | 255.255.255.255| Broadcast `FF:FF:FF:FF:FF` |
| 6     | DHCP offer    | 10.2.1.12 | 00:0c:29:a8:2b:bb         | 10.2.1.50      | 00:0c:29:90:1b:84          |
| 7     | DHCP requets  | 0.0.0.0   | 00:0c:29:90:1b:84         | 255.255.255.255| Broadcast `FF:FF:FF:FF:FF` |
| 8     | DHCP ack      | 10.2.1.12 | 00:0c:29:a8:2b:bb         | 10.2.1.50      | 00:0c:29:90:1b:84          |
| 9     | Requête ARP   | x         | 00:0c:29:90:1b:84         | x              | Broadcast `FF:FF:FF:FF:FF` |
| 10    | Requête ARP   | x         | 00:0c:29:90:1b:84         | x              | Broadcast `FF:FF:FF:FF:FF` |
| 11    | Requête ARP   | x         | 00:0c:29:a8:2b:bb         | x              | 00:0c:29:90:1b:84          |
| 12    | Requête ARP   | x         | 00:0c:29:a8:2b:bb         | x              | 00:0c:29:90:1b:84          |
| 13    | Réponse ARP   | x         | 00:0c:29:90:1b:84         | x              | 00:0c:29:a8:2b:bb          |
