# TP4 : Vers un réseau d'entreprise

## I. Dumb switch

## 3. Setup topologie 1

### 🌞 Commençons simple

définissez les IPs statiques sur les deux VPCS

ping un VPCS depuis l'autre

```bash
pc1> show

NAME   IP/MASK              GATEWAY           MAC                LPORT  RHOST:PORT
pc1    10.1.1.1/24          255.255.255.0     00:50:79:66:68:00  20006  127.0.0.1:20007
       fe80::250:79ff:fe66:6800/64

pc1> ping 10.1.1.2

84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=4.646 ms
84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=15.793 ms
84 bytes from 10.1.1.2 icmp_seq=3 ttl=64 time=2.349 ms
84 bytes from 10.1.1.2 icmp_seq=4 ttl=64 time=4.099 ms
84 bytes from 10.1.1.2 icmp_seq=5 ttl=64 time=8.255
```

## II. VLAN

## 3. Setup topologie 2

### 🌞 Adressage

vérifiez avec des ping que tout le monde se ping

```bash
pc1> show

NAME   IP/MASK              GATEWAY           MAC                LPORT  RHOST:PORT
pc1    10.1.1.1/24          255.255.255.0     00:50:79:66:68:00  20006  127.0.0.1:20007
       fe80::250:79ff:fe66:6800/64

pc1> ping 10.1.1.2

84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=4.646 ms
84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=15.793 ms
84 bytes from 10.1.1.2 icmp_seq=3 ttl=64 time=2.349 ms
84 bytes from 10.1.1.2 icmp_seq=4 ttl=64 time=4.099 ms
84 bytes from 10.1.1.2 icmp_seq=5 ttl=64 time=8.255 ms

pc1> ping 10.1.1.3

84 bytes from 10.1.1.3 icmp_seq=1 ttl=64 time=8.052 ms
84 bytes from 10.1.1.3 icmp_seq=2 ttl=64 time=5.393 ms
84 bytes from 10.1.1.3 icmp_seq=3 ttl=64 time=5.611 ms
84 bytes from 10.1.1.3 icmp_seq=4 ttl=64 time=3.041 ms
84 bytes from 10.1.1.3 icmp_seq=5 ttl=64 time=3.592 ms
```

### 🌞 Configuration des VLANs

déclaration des VLANs sur le switch sw1

```bash
Switch>enable
Switch#conf t
Enter configuration commands, one per line.  End with CNTL/Z.

Switch(config)#vlan 10
Switch(config-vlan)#name admins
Switch(config-vlan)#exit
Switch(config)#vlan 20
Switch(config-vlan)#name guests
Switch(config-vlan)#exit
Switch(config)#do show vlan

VLAN Name                             Status    Ports
---- -------------------------------- --------- -------------------------------
1    default                          active    Gi0/0, Gi0/1, Gi0/2, Gi0/3
                                                Gi1/0, Gi1/1, Gi1/2, Gi1/3
                                                Gi2/0, Gi2/1, Gi2/2, Gi2/3
                                                Gi3/0, Gi3/1, Gi3/2, Gi3/3
10   admins                           active    
20   guests                           active    
1002 fddi-default                     act/unsup 
1003 token-ring-default               act/unsup 
1004 fddinet-default                  act/unsup 
1005 trnet-default                    act/unsup 

VLAN Type  SAID       MTU   Parent RingNo BridgeNo Stp  BrdgMode Trans1 Trans2
---- ----- ---------- ----- ------ ------ -------- ---- -------- ------ ------
1    enet  100001     1500  -      -      -        -    -        0      0   
10   enet  100010     1500  -      -      -        -    -        0      0   
20   enet  100020     1500  -      -      -        -    -        0      0   
1002 fddi  101002     1500  -      -      -        -    -        0      0   
1003 tr    101003     1500  -      -      -        -    -        0      0   
1004 fdnet 101004     1500  -      -      -        ieee -        0      0   
1005 trnet 101005     1500  -      -      -        ibm  -        0      0   
          
Remote SPAN VLANs
------------------------------------------------------------------------------


Primary Secondary Type              Ports
------- --------- ----------------- ------------------------------------------

Switch(config)#int GigabitEthernet0/2
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 10
Switch(config-if)#int GigabitEthernet0/3   
Switch(config-if)#switchport access vlan 10
Switch(config-if)#int GigabitEthernet1/0   
Switch(config-if)#switchport access vlan 20
Switch(config-if)#do copy run start
Destination filename [startup-config]? 
Building configuration...
Compressed configuration from 3607 bytes to 1620 bytes[OK]
Switch(config-if)#
*Oct 18 10:04:08.990: %GRUB-5-CONFIG_WRITING: GRUB configuration is being updated on disk. Please wait...
*Oct 18 10:04:09.770: %GRUB-5-CONFIG_WRITTEN: GRUB configuration was written to disk successfully.
```

### 🌞 Vérif

pc1 et pc2 doivent toujours pouvoir se ping

pc3 ne ping plus personne

```bash
pc1> show         

NAME   IP/MASK              GATEWAY           MAC                LPORT  RHOST:PORT
pc1    10.1.1.1/24          255.255.255.0     00:50:79:66:68:00  20006  127.0.0.1:20007
       fe80::250:79ff:fe66:6800/64

pc1> ping 10.1.1.2

84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=7.589 ms
84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=3.281 ms
84 bytes from 10.1.1.2 icmp_seq=3 ttl=64 time=3.710 ms
84 bytes from 10.1.1.2 icmp_seq=4 ttl=64 time=2.262 ms
84 bytes from 10.1.1.2 icmp_seq=5 ttl=64 time=3.749 ms

pc1> ping 10.1.1.3

host (10.1.1.3) not reachable
```

## III. Routing

### 3. Setup topologie 3

### 🌞 Adressage

définissez les IPs statiques sur toutes les machines sauf le routeur

### 🌞 Configuration des VLANs

référez-vous au mémo Cisco

déclaration des VLANs sur le switch sw1

ajout des ports du switches dans le bon VLAN (voir le tableau d'adressage de la topo 2 juste au dessus)
il faudra ajouter le port qui pointe vers le routeur comme un trunk : c'est un port entre deux équipements réseau (un switch et un routeur)

```bash
Switch#conf t
Enter configuration commands, one per line.  End with CNTL/Z.
Switch(config)#vlan 11
Switch(config-vlan)#name clients
Switch(config-vlan)#vlan 12
Switch(config-vlan)#name admins
Switch(config-vlan)#vlan 13     
Switch(config-vlan)#name servers
Switch(config-vlan)#inter g0/2
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 11
Switch(config)#inter g0/3
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 11
Switch(config-if)#inter g1/0
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 12
Switch(config-if)#inter g0/1    
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 13
# trunk
Switch(config-if)#inter g0/0
Switch(config-if)#switchport trunk encapsulation dot1q
Switch(config-if)#switchport mode trunk
Switch(config-if)#switchport trunk allowed vlan add 11,12,13
Switch(config-if)#exit
Switch(config)#exit
```

### 🌞 Config du routeur

attribuez ses IPs au routeur

3 sous-interfaces, chacune avec son IP et un VLAN associé

```bash
R1(config-subif)#inter f0/0.11
R1(config-subif)#encapsulation dot1Q 11          
R1(config-subif)#ip addr 10.1.1.254 255.255.255.0
R1(config-subif)#inter f0/0.12                   
R1(config-subif)#encapsulation dot1Q 12          
R1(config-subif)#ip addr 10.2.2.254 255.255.255.0
R1(config-subif)#inter f0/0.13                   
R1(config-subif)#encapsulation dot1Q 13          
R1(config-subif)#ip addr 10.3.3.254 255.255.255.0
```

### 🌞 Vérif

tout le monde doit pouvoir ping le routeur sur l'IP qui est dans son réseau
en ajoutant une route vers les réseaux, ils peuvent se ping entre eux

ajoutez une route par défaut sur les VPCS
ajoutez une route par défaut sur la machine virtuelle
testez des ping entre les réseaux

```bash
# client 1
pc1> show

NAME   IP/MASK              GATEWAY           MAC                LPORT  RHOST:PORT
pc1    10.1.1.1/24          255.255.255.0     00:50:79:66:68:00  20013  127.0.0.1:20014
       fe80::250:79ff:fe66:6800/64

pc1> ping 10.1.1.254 

84 bytes from 10.1.1.254 icmp_seq=1 ttl=255 time=9.157 ms
84 bytes from 10.1.1.254 icmp_seq=2 ttl=255 time=16.224 ms
84 bytes from 10.1.1.254 icmp_seq=3 ttl=255 time=12.484 ms
84 bytes from 10.1.1.254 icmp_seq=4 ttl=255 time=11.407 ms
84 bytes from 10.1.1.254 icmp_seq=5 ttl=255 time=11.326 ms
```

```bash
# client 2
pc2> show

NAME   IP/MASK              GATEWAY           MAC                LPORT  RHOST:PORT
pc2    10.1.1.1/24          255.255.255.0     00:50:79:66:68:00  20013  127.0.0.1:20014
       fe80::250:79ff:fe66:6800/64

pc2> ping 10.1.1.254

84 bytes from 10.1.1.254 icmp_seq=1 ttl=255 time=11.909 ms
84 bytes from 10.1.1.254 icmp_seq=2 ttl=255 time=19.000 ms
84 bytes from 10.1.1.254 icmp_seq=3 ttl=255 time=17.625 ms
84 bytes from 10.1.1.254 icmp_seq=4 ttl=255 time=12.473 ms
84 bytes from 10.1.1.254 icmp_seq=5 ttl=255 time=11.756 ms
```

```bash
# admin
adm1> show

NAME   IP/MASK              GATEWAY           MAC                LPORT  RHOST:PORT
adm1   10.2.2.1/24          255.255.255.0     00:50:79:66:68:02  20047  127.0.0.1:20048
       fe80::250:79ff:fe66:6802/64

adm1> ping 10.2.2.254

84 bytes from 10.2.2.254 icmp_seq=1 ttl=255 time=7.288 ms
84 bytes from 10.2.2.254 icmp_seq=2 ttl=255 time=12.225 ms
84 bytes from 10.2.2.254 icmp_seq=3 ttl=255 time=14.934 ms
84 bytes from 10.2.2.254 icmp_seq=4 ttl=255 time=17.400 ms
84 bytes from 10.2.2.254 icmp_seq=5 ttl=255 time=12.908 ms
```

```bash
# web
[kaddate@web ~]$ ping 10.3.3.254 -c3 | grep "packet loss"
3 packets transmitted, 3 received, 0% packet loss, time 2003ms
```

## IV. NAT

## 3. Setup topologie 4

### 🌞 Ajoutez le noeud Cloud à la topo

branchez à eth1 côté Cloud

côté routeur, il faudra récupérer un IP en DHCP (voir le mémo Cisco)

vous devriez pouvoir ping 1.1.1.1

```bash
R1(config)#int f1/0
R1(config-if)#no shutdown
R1(config-if)#ip addr dhcp
R1(config-if)#
*Mar  1 00:05:00.319: %LINK-3-UPDOWN: Interface FastEthernet1/0, changed state to up
*Mar  1 00:05:01.319: %LINEPROTO-5-UPDOWN: Line protocol on Interface FastEthernet1/0, changed state to up
R1(config-if)#
R1(config-if)#
R1(config-if)#do sh int
*Mar  1 00:05:10.415: %DHCP-6-ADDRESS_ASSIGN: Interface FastEthernet1/0 assigned DHCP address 10.0.3.16, mask 255.255.255.0, hostname R1

R1(config-if)#do ping 1.1.1.1

Type escape sequence to abort.
Sending 5, 100-byte ICMP Echos to 1.1.1.1, timeout is 2 seconds:
.!!!!
Success rate is 80 percent (4/5), round-trip min/avg/max = 48/60/68 ms
```

### 🌞 Configurez le NAT

référez-vous à la section NAT du mémo Cisco

```bash
R1(config-if)#int f0/0
R1(config-if)#ip nat inside

*Mar  1 00:37:44.275: %LINEPROTO-5-UPDOWN: Line protocol on Interface NVI0, changed state to up
R1(config-if)#
R1(config-if)#int f1/0
R1(config-if)#ip nat outside
R1(config-if)#exit
R1(config)#access-list 1 permit any
R1(config)#ip nat inside source list 1 interface f1/0 overload 
```

### 🌞 Test

vérifiez un ping vers un nom de domaine

```bash
# web
[kaddate@web ~]$ ping ynov.com -c3 | grep "packet loss"
3 packets transmitted, 3 received, 0% packet loss, time 2006ms
#pc1
pc1> ping ynov.com 
ynov.com resolved to 92.243.16.143

84 bytes from 92.243.16.143 icmp_seq=1 ttl=61 time=28.986 ms
84 bytes from 92.243.16.143 icmp_seq=2 ttl=61 time=29.429 ms
84 bytes from 92.243.16.143 icmp_seq=3 ttl=61 time=35.856 ms
84 bytes from 92.243.16.143 icmp_seq=4 ttl=61 time=29.582 ms
84 bytes from 92.243.16.143 icmp_seq=5 ttl=61 time=37.504 ms
# admin
adm1> ping ynov.com             
ynov.com resolved to 92.243.16.143

84 bytes from 92.243.16.143 icmp_seq=1 ttl=61 time=39.934 ms
84 bytes from 92.243.16.143 icmp_seq=2 ttl=61 time=27.351 ms
84 bytes from 92.243.16.143 icmp_seq=3 ttl=61 time=32.844 ms
84 bytes from 92.243.16.143 icmp_seq=4 ttl=61 time=34.686 ms
84 bytes from 92.243.16.143 icmp_seq=5 ttl=61 time=34.118 ms
```

## V. Add a building

### 3. Setup topologie 5

### 🌞  Vous devez me rendre le show running-config de tous les équipements

**switch1**

```bash
show run   
Building configuration...

Current configuration : 3773 bytes
!
! Last configuration change at 19:31:34 UTC Fri Oct 22 2021
!
version 15.2
service timestamps debug datetime msec
service timestamps log datetime msec
no service password-encryption
service compress-config
!
hostname Switch
!
boot-start-marker
boot-end-marker
!
!
!
no aaa new-model
!
!
!
!
!         
!
!
!
ip cef
no ipv6 cef
!
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
vlan internal allocation policy ascending
!
! 
!
!
!
!
!
!
!
!
!         
!
!
!
interface GigabitEthernet0/0
 switchport trunk encapsulation dot1q
 switchport mode trunk
 media-type rj45
 negotiation auto
!
interface GigabitEthernet0/1
 switchport mode access
 media-type rj45
 negotiation auto
!
interface GigabitEthernet0/1.2
!
interface GigabitEthernet0/2
 switchport trunk encapsulation dot1q
 switchport mode trunk
 media-type rj45
 negotiation auto
!
interface GigabitEthernet0/3
 switchport trunk encapsulation dot1q
 switchport mode trunk
 media-type rj45
 negotiation auto
!
interface GigabitEthernet1/0
 switchport mode access
 media-type rj45
 negotiation auto
!
interface GigabitEthernet1/1
 media-type rj45
 negotiation auto
!
interface GigabitEthernet1/2
 media-type rj45
 negotiation auto
!
interface GigabitEthernet1/3
 media-type rj45
 negotiation auto
!
interface GigabitEthernet2/0
 media-type rj45
 negotiation auto
!
interface GigabitEthernet2/1
 media-type rj45
 negotiation auto
!
interface GigabitEthernet2/2
 media-type rj45
 negotiation auto
!
interface GigabitEthernet2/3
 media-type rj45
 negotiation auto
!
interface GigabitEthernet3/0
 media-type rj45
 negotiation auto
!
interface GigabitEthernet3/1
 media-type rj45
 negotiation auto
!         
interface GigabitEthernet3/2
 media-type rj45
 negotiation auto
!
interface GigabitEthernet3/3
 media-type rj45
 negotiation auto
!
ip forward-protocol nd
!
no ip http server
no ip http secure-server
!
!
!
!
!
!
control-plane
!
banner exec ^C
**************************************************************************
* IOSv is strictly limited to use for evaluation, demonstration and IOS  *
* education. IOSv is provided as-is and is not supported by Cisco's      *
* Technical Advisory Center. Any use or disclosure, in whole or in part, *
* of the IOSv Software or Documentation to any third party for any       *
* purposes is expressly prohibited except as otherwise authorized by     *
* Cisco in writing.                                                      *
**************************************************************************^C
banner incoming ^C
**************************************************************************
* IOSv is strictly limited to use for evaluation, demonstration and IOS  *
* education. IOSv is provided as-is and is not supported by Cisco's      *
* Technical Advisory Center. Any use or disclosure, in whole or in part, *
* of the IOSv Software or Documentation to any third party for any       *
* purposes is expressly prohibited except as otherwise authorized by     *
* Cisco in writing.                                                      *
**************************************************************************^C
banner login ^C
**************************************************************************
* IOSv is strictly limited to use for evaluation, demonstration and IOS  *
* education. IOSv is provided as-is and is not supported by Cisco's      *
* Technical Advisory Center. Any use or disclosure, in whole or in part, *
* of the IOSv Software or Documentation to any third party for any       *
* purposes is expressly prohibited except as otherwise authorized by     *
* Cisco in writing.                                                      *
**************************************************************************^C
!
line con 0
line aux 0
line vty 0 4
 login
!
!
end
```

**switch2**

```bash
Switch(config-if)#do sh run
Building configuration...

Current configuration : 3706 bytes
!
! Last configuration change at 19:38:42 UTC Fri Oct 22 2021
!
version 15.2
service timestamps debug datetime msec
service timestamps log datetime msec
no service password-encryption
service compress-config
!
hostname Switch
!
boot-start-marker
boot-end-marker
!
!
!
no aaa new-model
!
!
!
!
!         
!
!
!
ip cef
no ipv6 cef
!
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
vlan internal allocation policy ascending
!
! 
!
!
!
!
!
!
!
!
!         
!
!
!
interface GigabitEthernet0/0
 media-type rj45
 negotiation auto
!
interface GigabitEthernet0/1
 switchport access vlan 11
 switchport mode access
 media-type rj45
 negotiation auto
!
interface GigabitEthernet0/2
 switchport access vlan 11
 switchport mode access
 media-type rj45
 negotiation auto
!
interface GigabitEthernet0/3
 switchport access vlan 12
 switchport mode access
 media-type rj45
 negotiation auto
!
interface GigabitEthernet1/0
 switchport access vlan 13
 switchport mode access
 media-type rj45
 negotiation auto
!
interface GigabitEthernet1/1
 media-type rj45
 negotiation auto
!
interface GigabitEthernet1/2
 media-type rj45
 negotiation auto
!
interface GigabitEthernet1/3
 media-type rj45
 negotiation auto
!
interface GigabitEthernet2/0
 media-type rj45
 negotiation auto
!
interface GigabitEthernet2/1
 media-type rj45
 negotiation auto
!
interface GigabitEthernet2/2
 media-type rj45
 negotiation auto
!
interface GigabitEthernet2/3
 media-type rj45
 negotiation auto
!
interface GigabitEthernet3/0
 media-type rj45
 negotiation auto
!
interface GigabitEthernet3/1
 media-type rj45
 negotiation auto
!
interface GigabitEthernet3/2
 media-type rj45
 negotiation auto
!
interface GigabitEthernet3/3
 media-type rj45
 negotiation auto
!
ip forward-protocol nd
!
no ip http server
no ip http secure-server
!
!
!
!
!
!
control-plane
!
banner exec ^C
**************************************************************************
* IOSv is strictly limited to use for evaluation, demonstration and IOS  *
* education. IOSv is provided as-is and is not supported by Cisco's      *
* Technical Advisory Center. Any use or disclosure, in whole or in part, *
* of the IOSv Software or Documentation to any third party for any       *
* purposes is expressly prohibited except as otherwise authorized by     *
* Cisco in writing.                                                      *
**************************************************************************^C
banner incoming ^C
**************************************************************************
* IOSv is strictly limited to use for evaluation, demonstration and IOS  *
* education. IOSv is provided as-is and is not supported by Cisco's      *
* Technical Advisory Center. Any use or disclosure, in whole or in part, *
* of the IOSv Software or Documentation to any third party for any       *
* purposes is expressly prohibited except as otherwise authorized by     *
* Cisco in writing.                                                      *
**************************************************************************^C
banner login ^C
**************************************************************************
* IOSv is strictly limited to use for evaluation, demonstration and IOS  *
* education. IOSv is provided as-is and is not supported by Cisco's      *
* Technical Advisory Center. Any use or disclosure, in whole or in part, *
* of the IOSv Software or Documentation to any third party for any       *
* purposes is expressly prohibited except as otherwise authorized by     *
* Cisco in writing.                                                      *
**************************************************************************^C
!         
line con 0
line aux 0
line vty 0 4
!
!
end
```

**switch3**

```bash
Switch(config-if)#do sh run
Building configuration...

Current configuration : 3706 bytes
!
! Last configuration change at 20:15:44 UTC Fri Oct 22 2021
!
version 15.2
service timestamps debug datetime msec
service timestamps log datetime msec
no service password-encryption
service compress-config
!
hostname Switch
!
boot-start-marker
boot-end-marker
!
!
!
no aaa new-model
!
!
!
!
!         
!
!
!
ip cef
no ipv6 cef
!
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
vlan internal allocation policy ascending
!
! 
!
!
!
!
!
!
!
!
!         
!
!
!
interface GigabitEthernet0/0
 media-type rj45
 negotiation auto
!
interface GigabitEthernet0/1
 switchport access vlan 11
 switchport mode access
 media-type rj45
 negotiation auto
!
interface GigabitEthernet0/2
 switchport access vlan 11
 switchport mode access
 media-type rj45
 negotiation auto
!
interface GigabitEthernet0/3
 switchport access vlan 11
 switchport mode access
 media-type rj45
 negotiation auto
!
interface GigabitEthernet1/0
 switchport access vlan 11
 switchport mode access
 media-type rj45
 negotiation auto
!
interface GigabitEthernet1/1
 media-type rj45
 negotiation auto
!
interface GigabitEthernet1/2
 media-type rj45
 negotiation auto
!
interface GigabitEthernet1/3
 media-type rj45
 negotiation auto
!
interface GigabitEthernet2/0
 media-type rj45
 negotiation auto
!
interface GigabitEthernet2/1
 media-type rj45
 negotiation auto
!
interface GigabitEthernet2/2
 media-type rj45
 negotiation auto
!
interface GigabitEthernet2/3
 media-type rj45
 negotiation auto
!
interface GigabitEthernet3/0
 media-type rj45
 negotiation auto
!
interface GigabitEthernet3/1
 media-type rj45
 negotiation auto
!
interface GigabitEthernet3/2
 media-type rj45
 negotiation auto
!
interface GigabitEthernet3/3
 media-type rj45
 negotiation auto
!
ip forward-protocol nd
!
no ip http server
no ip http secure-server
!
!
!
!
!
!
control-plane
!
banner exec ^C
**************************************************************************
* IOSv is strictly limited to use for evaluation, demonstration and IOS  *
* education. IOSv is provided as-is and is not supported by Cisco's      *
 --More-- 
*Oct 22 20:21:44.774: %LINEPROTO-5-UPDOWN: Line protocol on Interface GigabitEthernet1/0, changed state to down
*Oct 22 20:21:45.795: %LINK-3-UPDOWN: Interface GigabitEthernet1/0, changed state to down
*Oct 22 20:21:58.785: %LINK-3-UPDOWN: Interface GigabitEthernet1/0, changed state to up
*Oct 22 20:21:59.785: %LINEPROTO-5-UPDOWN: Line protocol on Interface GigabitEthernet1/0, changed s* Technical Advisory Center. Any use or disclosure, in whole or in part, *
* of the IOSv Software or Documentation to any third party for any       *
* purposes is expressly prohibited except as otherwise authorized by     *
* Cisco in writing.                                                      *
**************************************************************************^C
banner incoming ^C
**************************************************************************
* IOSv is strictly limited to use for evaluation, demonstration and IOS  *
* education. IOSv is provided as-is and is not supported by Cisco's      *
* Technical Advisory Center. Any use or disclosure, in whole or in part, *
* of the IOSv Software or Documentation to any third party for any       *
* purposes is expressly prohibited except as otherwise authorized by     *
* Cisco in writing.                                                      *
**************************************************************************^C
banner login ^C
**************************************************************************
* IOSv is strictly limited to use for evaluation, demonstration and IOS  *
* education. IOSv is provided as-is and is not supported by Cisco's      *
* Technical Advisory Center. Any use or disclosure, in whole or in part, *
* of the IOSv Software or Documentation to any third party for any       *
* purposes is expressly prohibited except as otherwise authorized by     *
* Cisco in writing.                                                      *
**************************************************************************^C
!
line con 0
line aux 0
line vty 0 4
!
!
end
```

**router**

```bash
R1#sh run
Building configuration...

Current configuration : 1356 bytes
!
version 12.4
service timestamps debug datetime msec
service timestamps log datetime msec
no service password-encryption
!
hostname R1
!
boot-start-marker
boot-end-marker
!
!
no aaa new-model
memory-size iomem 5
no ip icmp rate-limit unreachable
!
!
ip cef
no ip domain lookup
!
!
!         
!
!
!
!
!
!
!
!
!
!
!
!
!
ip tcp synwait-time 5
!
!
!
interface FastEthernet0/0
 no ip address
 ip nat inside
 ip virtual-reassembly
 duplex auto
 speed auto
!
interface FastEthernet0/0.11
 encapsulation dot1Q 11
 ip address 10.1.1.254 255.255.255.0
!
interface FastEthernet0/0.12
 encapsulation dot1Q 12
 ip address 10.2.2.254 255.255.255.0
!
interface FastEthernet0/0.13
 encapsulation dot1Q 13
 ip address 10.3.3.254 255.255.255.0
!
interface FastEthernet1/0
 ip address dhcp
 ip nat outside
 ip virtual-reassembly
 duplex auto
 speed auto
!
interface FastEthernet2/0
 no ip address
 shutdown 
 duplex auto
 speed auto
!
interface FastEthernet3/0
 no ip address
 shutdown
 duplex auto
 speed auto
!
!
no ip http server
ip forward-protocol nd
!
!
ip nat inside source list 1 interface FastEthernet1/0 overload
!
access-list 1 permit any
no cdp log mismatch duplex
!
!
!
control-plane
!         
!
!
!
!
!
!
!
!
line con 0
 exec-timeout 0 0
 privilege level 15
 logging synchronous
line aux 0
 exec-timeout 0 0
 privilege level 15
 logging synchronous
line vty 0 4
 login
!
!
end
```

### 🌞  Mettre en place un serveur DHCP dans le nouveau bâtiment

avoir une IP dans le réseau clients

avoir un accès au réseau servers

avoir un accès WAN
avoir de la résolution DNS

```bash

```

```bash

```

```bash

```

```bash

```