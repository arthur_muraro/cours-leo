# TP1 - Mise en jambes

##  1. Affichage d'informations sur la pile TCP/IP locale

## En Ligne de Commande

### 🌞 Affichez les infos des cartes réseau de votre PC

j'utilise la commande `ip a`
```
interface wifi :
  nom : wlp0s20f3
  MAC : 34:c9:3d:4d:a3:78
  ip : 10.33.2.221/22

interface ether :
  je n'ai pas d'interface ethernet.
```

### 🌞 Affichez votre gateway

J'utilise la commande :
```bash
ip r | grep default | cut -d' ' -f3
```
qui me renvoie `10.33.3.253`.

## En graphique (GUI : Graphical User Interface)

### 🌞 Trouvez comment afficher les informations sur une carte IP (change selon l'OS)

![si cette image ne s'affiche pas, le fichier est présent dans le meme dossier que le markdown au nom de "network-manager.png"](./network-manager.png?raw=true)

### 🌞à quoi sert la gateway dans le réseau d'YNOV ?

Elle sert de passerelle entre le réseau local d'ynov et internet (il est aussi possible qu'elle ramène dans un autre réseau privé d'ynov, avant de faire le transfert vers internet).

## 2. Modifications des informations

## A. Modification d'adresse IP (part 1)

### 🌞 Utilisez l'interface graphique de votre OS pour changer d'adresse IP

![si cette image ne s'affiche pas, le fichier est présent dans le meme dossier que le markdown au nom de "changment-manuel.png"](./changment-manuel.png?raw=true)

### 🌞 Il est possible que vous perdiez l'accès internet. Que ce soit le cas ou non, expliquez pourquoi c'est possible de perdre son accès internet en faisant cette opération

il est possible que la gateway n'accepte pas cette adresse étant donnné qu'il n'y a pas de bail reservé dessus. il est aussi possible que l'adresse soit déjà présente sur le réseau ce qui créerait un conflit d'addresse ip.

## B. Table ARP

### 🌞 Exploration de la table ARP

pour afficher la table arp, j'utilise la commande `arp`.

L'addresse MAC de ma gateway est `00:12:00:40:4c:bf`, elle est facilemment reconnaissable étant donné que c'est précisé dans l'output de la commande.

### 🌞 Et si on remplissait un peu la table ?

voici les adresses que j'ai ping et qui sont affiché par ARP :
```
10.33.1.103              ether   18:65:90:ce:f5:63   C                     wlp0s20f3
10.33.1.76               ether   e0:cc:f8:7a:4f:4a   C                     wlp0s20f3
```

## C. nmap

### 🌞 Utilisez nmap pour scanner le réseau de votre carte WiFi et trouver une adresse IP libre

voici une partie d'un output d'un ScanPing sur le réseau
```
Nmap scan report for 10.33.0.7
Host is up (0.28s latency).
```

pour afficher ma table ARP je tape la commande `arp` :
je peux constater qu'elle contient maintenant toutes les machines présentent sur le réseau.

## D. Modification d'adresse IP (part 2)

### 🌞 Modifiez de nouveau votre adresse IP vers une adresse IP que vous savez libre grâce à nmap

```bash
sudo ifconfig  wlp0s20f3 10.33.0.118 netmask 255.255.252.0 up
sudo route add default gw 10.33.3.253
```

#### utilisez un ping scan sur le réseau YNOV, montrez moi la commande nmap et son résultat.

```bash
~ $ nmap -sP 10.33.0.0/22
Starting Nmap 7.92 ( https://nmap.org ) at 2021-09-16 09:38 CEST
Nmap scan report for 10.33.0.7
Host is up (0.35s latency).
Nmap scan report for 10.33.0.19
Host is up (0.095s latency).
Nmap scan report for 10.33.0.31
Host is up (0.48s latency).
Nmap scan report for 10.33.0.59
Host is up (0.25s latency).
Nmap scan report for 10.33.0.60
Host is up (0.11s latency).
Nmap scan report for 10.33.0.96
Host is up (0.14s latency).
Nmap scan report for 10.33.0.117
Host is up (0.55s latency).
Nmap scan report for kaddateThinkPad (10.33.0.118)
Host is up (0.00021s latency).
Nmap scan report for 10.33.0.119
[...]
Host is up (0.010s latency).
Nmap scan report for 10.33.0.135
Host is up (0.0097s latency).
Nmap scan report for 10.33.0.143
Host is up (0.024s latency).
Nmap scan report for 10.33.0.180
Host is up (0.18s latency).
Nmap scan report for 10.33.0.212
Host is up (0.12s latency).
Nmap scan report for 10.33.0.250
Host is up (0.013s latency).
Nmap scan report for 10.33.1.23
Host is up (0.15s latency).
Nmap scan report for _gateway (10.33.3.253)
Host is up (0.024s latency).
Nmap done: 1024 IP addresses (31 hosts up) scanned in 44.78 seconds
```

#### configurez correctement votre gateway pour avoir accès à d'autres réseaux (utilisez toujours la gateway d'YNOV)

#### prouvez en une suite de commande que vous avez une IP choisi manuellement, que votre passerelle est bien définie, et que vous avez un accès internet

informations ip
```bash
ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: wlp0s20f3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default qlen 1000
    link/ether 34:c9:3d:4d:a3:78 brd ff:ff:ff:ff:ff:ff
    inet 10.33.0.118/22 brd 10.33.3.255 scope global wlp0s20f3
       valid_lft forever preferred_lft forever
    inet6 fe80::dd7e:4084:e249:f0c9/64 scope link noprefixroute 
       valid_lft forever preferred_lft forever
3: docker0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc noqueue state DOWN group default 
    link/ether 02:42:3f:f0:04:60 brd ff:ff:ff:ff:ff:ff
    inet 172.17.0.1/16 brd 172.17.255.255 scope global docker0
       valid_lft forever preferred_lft forever
4: vmnet1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UNKNOWN group default qlen 1000
    link/ether 00:50:56:c0:00:01 brd ff:ff:ff:ff:ff:ff
    inet 192.168.245.1/24 brd 192.168.245.255 scope global vmnet1
       valid_lft forever preferred_lft forever
    inet6 fe80::250:56ff:fec0:1/64 scope link 
       valid_lft forever preferred_lft forever
5: vmnet8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UNKNOWN group default qlen 1000
    link/ether 00:50:56:c0:00:08 brd ff:ff:ff:ff:ff:ff
    inet 192.168.138.1/24 brd 192.168.138.255 scope global vmnet8
       valid_lft forever preferred_lft forever
    inet6 fe80::250:56ff:fec0:8/64 scope link 
       valid_lft forever preferred_lft forever
```

```bash
➜  ~ ip r | grep default
default via 10.33.3.253 dev wlp0s20f3 
default via 10.33.3.253 dev wlp0s20f3 proto static metric 600

➜  ~ ping 8.8.8.8 -c4        
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 octets de 8.8.8.8 : icmp_seq=1 ttl=115 temps=20.2 ms
64 octets de 8.8.8.8 : icmp_seq=2 ttl=115 temps=26.4 ms
64 octets de 8.8.8.8 : icmp_seq=3 ttl=115 temps=24.6 ms
64 octets de 8.8.8.8 : icmp_seq=4 ttl=115 temps=138 ms

--- statistiques ping 8.8.8.8 ---
4 paquets transmis, 4 reçus, 0 % paquets perdus, temps 3005 ms
rtt min/avg/max/mdev = 20.231/52.274/137.858/49.462 ms
```

## II. Exploration locale en duo

## j'ai précisé plus haut que je n'avais pas de carte ethernet, mais j'ai obtenu un adaptateur USB-C vers ethernet entre temps.
## pour la partie 2 je suis avec clément, et avec graig (qui n'a pas de port rj45)

### 🌞 Si vos PCs ont un port RJ45 alors y'a une carte réseau Ethernet associée

## 3. Modification d'adresse IP

```bash
➜  ~ arp | grep 192.168.10
192.168.10.1             ether   98:28:a6:2c:17:f0   C                     enx44a92c500340

➜  ~ ip a
11: enx44a92c500340: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 44:a9:2c:50:03:40 brd ff:ff:ff:ff:ff:ff
    inet 192.168.10.2/30 brd 192.168.10.3 scope global noprefixroute enx44a92c500340
       valid_lft forever preferred_lft forever
    inet6 fe80::21af:d2f4:7fda:1bde/64 scope link noprefixroute 
       valid_lft forever preferred_lft forever

➜  ~ ping 192.168.10.1 -c2
PING 192.168.10.2 (192.168.10.2) 56(84) bytes of data.
64 octets de 192.168.10.2 : icmp_seq=1 ttl=64 temps=0.068 ms
64 octets de 192.168.10.2 : icmp_seq=2 ttl=64 temps=0.063 ms

--- statistiques ping 192.168.10.2 ---
2 paquets transmis, 2 reçus, 0 % paquets perdus, temps 1031 ms
rtt min/avg/max/mdev = 0.063/0.065/0.068/0.002 ms
```

## 4. Utilisation d'un des deux comme gateway

### sur le PC qui n'a plus internet

désactivez l'interface WiFi

s'assurer de la bonne connectivité entre les deux PCs à travers le câble RJ45

sur la carte Ethernet, définir comme passerelle l'adresse IP de l'autre PC

```bash
sudo ifconfig wlp0s20f3 down

➜  ~ ping 192.168.10.1 -c2
PING 192.168.10.1 (192.168.10.1) 56(84) bytes of data.
64 octets de 192.168.10.1 : icmp_seq=1 ttl=128 temps=1.11 ms
64 octets de 192.168.10.1 : icmp_seq=2 ttl=128 temps=0.880 ms

--- statistiques ping 192.168.10.1 ---
2 paquets transmis, 2 reçus, 0 % paquets perdus, temps 1001 ms
rtt min/avg/max/mdev = 0.880/0.993/1.106/0.113 ms

➜  ~ ip r | grep default
default via 192.168.10.1 dev enx44a92c500340 proto static metric 100 
```

### 🌞 pour tester la connectivité à internet on fait souvent des requêtes simples vers un serveur internet connu

```bash
➜  ~ ping -c4 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 octets de 8.8.8.8 : icmp_seq=1 ttl=114 temps=19.9 ms
64 octets de 8.8.8.8 : icmp_seq=2 ttl=114 temps=19.3 ms
64 octets de 8.8.8.8 : icmp_seq=3 ttl=114 temps=20.6 ms
64 octets de 8.8.8.8 : icmp_seq=4 ttl=114 temps=17.8 ms

--- statistiques ping 8.8.8.8 ---
4 paquets transmis, 4 reçus, 0 % paquets perdus, temps 3005 ms
rtt min/avg/max/mdev = 17.788/19.395/20.603/1.035 ms
```

### 🌞 utiliser un traceroute ou tracert pour bien voir que les requêtes passent par la passerelle choisie (l'autre le PC)

```bash
➜  ~ traceroute 8.8.8.8         
traceroute to 8.8.8.8 (8.8.8.8), 30 hops max, 60 byte packets
 1  _gateway (192.168.137.1)  0.581 ms  0.486 ms  0.448 ms
 2  * * *
 3  _gateway (10.33.3.253)  4.615 ms  6.566 ms  8.490 ms
 4  10.33.10.254 (10.33.10.254)  8.199 ms  8.582 ms  9.745 ms
 5  reverse.completel.net (92.103.174.137)  10.634 ms  13.566 ms  13.850 ms
 6  92.103.120.182 (92.103.120.182)  15.610 ms  9.026 ms  9.270 ms
 7  172.19.130.117 (172.19.130.117)  18.431 ms  23.411 ms  23.367 ms
 8  46.218.128.74 (46.218.128.74)  19.149 ms  19.282 ms  22.360 ms
 9  38.147.6.194.rev.sfr.net (194.6.147.38)  22.840 ms  22.626 ms  23.455 ms
10  72.14.194.30 (72.14.194.30)  21.690 ms  22.703 ms  17.326 ms
11  * * *
12  dns.google (8.8.8.8)  33.396 ms  30.249 ms  31.058 ms
```

## 5. Petit chat privé

### 🌞 sur le PC serveur avec l'IP 192.168.137.2

```bash
➜  ~ netcat -l -p 8888 
salut
Léo c'est le plus beau
tu trouves ? jprefere devilledon
Oh le batart
allez jme casse t'as des gouts de chiottes
```

### 🌞 sur le PC client avec l'IP 192.168.137.1

commande de clément : (c'est lui le client) : `nc.exe 192.168.137.2 8888`

### 🌞 pour aller un peu plus loin

commande de clément : (c'est lui le serveur) : `nc.exe -l -p 192.168.137.2 8888`

```bash
➜  ~ netcat 192.168.137.1 8888
hello
ciao !
```

## 6. Firewall

### 🌞 Autoriser les ping

### 🌞 Autoriser nc sur un port spécifique

voici le statut de mon firewall après les changements demandé, j'ai autorisé le port 1050 pour netcat.

```bash
➜  ~ sudo ufw status verbose               
État : actif
Journalisation : on (low)
Par défaut : deny (incoming), allow (outgoing), deny (routed)
Nouveaux profils : skip

Vers                       Action      De
----                       ------      --
7                          ALLOW IN    Anywhere                  
1050                       ALLOW IN    Anywhere                  
7 (v6)                     ALLOW IN    Anywhere (v6)             
1050 (v6)                  ALLOW IN    Anywhere (v6)
```

voici la connexion netcat

```bash
➜  ~ netcat 192.168.137.1 1050
ciao !un ruc propre
```

## III. Manipulations d'autres outils/protocoles côté client

(les manipulations effectué du dernier chapitres sont effectués sur mon réseau domestique ).

## 1. DHCP

### 🌞 Exploration du DHCP, depuis votre PC

afficher l'adresse IP du serveur DHCP du réseau WiFi YNOV

```bash
➜  ~ sudo dhclient -d -nw wlp0s20f3
[...]
DHCPDISCOVER on wlp0s20f3 to 255.255.255.255 port 67 interval 3 (xid=0xe223845e)
DHCPOFFER of 10.33.0.109 from 10.33.3.254
[...]
```
cette adresse a une durée de vie limitée. C'est le principe du bail DHCP (ou DHCP lease). Trouver la date d'expiration de votre bail DHCP

```bash
➜  ~ cat /var/log/syslog | grep dhcp_lease_time | tail -n 1 | cut -d'>' -f3

 '43200'
```

(mon bail dure 43200 secondes soit 12 heures)

## 2. DNS

### 🌞 trouver l'adresse IP du serveur DNS que connaît votre ordinateur

```bash
➜  ~ cat /var/log/syslog | grep ' domain_name_server' | tail -n 1 | cut -d'>' -f3

 '192.168.7.254'
```

### 🌞 utiliser, en ligne de commande l'outil nslookup (Windows, MacOS) ou dig (GNU/Linux, MacOS) pour faire des requêtes DNS à la main

### pour google.com

```bash
➜  ~ dig google.com        

; <<>> DiG 9.16.1-Ubuntu <<>> google.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 41153
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 65494
;; QUESTION SECTION:
;google.com.			IN	A

;; ANSWER SECTION:
google.com.		42	IN	A	216.58.213.142

;; Query time: 0 msec
;; SERVER: 127.0.0.53#53(127.0.0.53)
;; WHEN: ven. sept. 17 21:25:52 CEST 2021
;; MSG SIZE  rcvd: 55
```

### pour ynov.com

```bash
➜  ~ dig ynov.com  

; <<>> DiG 9.16.1-Ubuntu <<>> ynov.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 11270
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 65494
;; QUESTION SECTION:
;ynov.com.			IN	A

;; ANSWER SECTION:
ynov.com.		6420	IN	A	92.243.16.143

;; Query time: 12 msec
;; SERVER: 127.0.0.53#53(127.0.0.53)
;; WHEN: ven. sept. 17 21:26:43 CEST 2021
;; MSG SIZE  rcvd: 53
```

dig est un outil qui permet de questionner son serveur DNS sur un nom de domaine, il affiche toutes les informations renvoyé par le DNS et pas seulement l'adresse du nom de domaine que l'on cherche.
Ici, on peut voir que le serveur qui m'a répondu est le 127.0.0.53, donc un serveur local, je n'ai pas trouvé la réponse sur internet mais je suppose que c'est mon "service DNS local" qui l'a gardé en mémoire (je pense simplement que c'est mon cache qui me répond, car la demande est régulièrement faite.)

### reverse request pour l'adresse 78.74.21.21

```bash
➜  ~ dig -x 78.74.21.21

; <<>> DiG 9.16.1-Ubuntu <<>> -x 78.74.21.21
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 39225
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 65494
;; QUESTION SECTION:
;21.21.74.78.in-addr.arpa.	IN	PTR

;; ANSWER SECTION:
21.21.74.78.in-addr.arpa. 3600	IN	PTR	host-78-74-21-21.homerun.telia.com.

;; Query time: 172 msec
;; SERVER: 127.0.0.53#53(127.0.0.53)
;; WHEN: ven. sept. 17 21:32:20 CEST 2021
;; MSG SIZE  rcvd: 101
```

### reverse request pour l'adresse 92.146.54.88

```bash
➜  ~ dig -x 92.146.54.88

; <<>> DiG 9.16.1-Ubuntu <<>> -x 92.146.54.88
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 47618
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 65494
;; QUESTION SECTION:
;88.54.146.92.in-addr.arpa.	IN	PTR

;; ANSWER SECTION:
88.54.146.92.in-addr.arpa. 3600	IN	PTR	apoitiers-654-1-167-88.w92-146.abo.wanadoo.fr.

;; Query time: 36 msec
;; SERVER: 127.0.0.53#53(127.0.0.53)
;; WHEN: ven. sept. 17 21:32:29 CEST 2021
;; MSG SIZE  rcvd: 113
```

comme son nom l'indique les reverse request permettent de faire l'inverse d'une request classique, ici j'ai donc traduit mes adresses IP en noms de domaines provenant de "telia.com" et "wanadoo.fr"

## IV. Wireshark

### 🌞 utilisez le pour observer les trames qui circulent entre vos deux carte Ethernet. Mettez en évidence

#### un ping entre vous et la passerelle

![si cette image ne s'affiche pas, le fichier est présent dans le meme dossier que le markdown au nom de "wireshark-ping-gateway.png"](./wireshark-ping-gateway.png?raw=true)

#### un netcat entre vous et votre mate, branché en RJ45

je suis tout seul donc je vais me netcat sur ma propre machine

partie serveur : `netcat -l -p 8888`

partie client : `netcat 127.0.0.1 8888`

![si cette image ne s'affiche pas, le fichier est présent dans le meme dossier que le markdown au nom de "wireshark-netcat.png"](./wireshark-netcat.png?raw=true)

#### une requête DNS. Identifiez dans la capture le serveur DNS à qui vous posez la question

![si cette image ne s'affiche pas, le fichier est présent dans le meme dossier que le markdown au nom de "wireshark-dns.png"](./wireshark-dns.png?raw=true)

# fin du TP !
