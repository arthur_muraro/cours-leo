# TP2 pt. 1 : Gestion de service

## I. Un premier serveur web

## 1. Installation

### 🌞 Installer le serveur Apache

paquet httpd

```bash
# commande d'installaion d'apache
sudo yum install httpd -y
```

la conf se trouve dans /etc/httpd/

le fichier de conf principal est /etc/httpd/conf/httpd.conf

je vous conseille vivement de virer tous les commentaire du fichier, à défaut de les lire, vous y verrez plus clair

```bash
[kaddate@node1 conf]$ sudo sed -i '/^ *#.*/d' httpd.conf
```

### 🌞 Démarrer le service Apache

démarrez le service httpd (raccourci pour httpd.service en réalité)

```bash
[kaddate@node1 conf]$ sudo systemctl start httpd
```

faites en sorte qu'Apache démarre automatiquement au démarrage de la machine

```bash
[kaddate@node1 conf]$ sudo systemctl enable httpd
Created symlink /etc/systemd/system/multi-user.target.wants/httpd.service → /usr/lib/systemd/system/httpd.service.
```

ouvrez le port firewall nécessaire

```bash
[kaddate@node1 conf]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
```

utiliser une commande ss pour savoir sur quel port tourne actuellement Apache

```bash
[kaddate@node1 conf]$ sudo ss -tanp | grep httpd
LISTEN 0      128                *:80              *:*     users:(("httpd",pid=2792,fd=4),("httpd",pid=2791,fd=4),("httpd",pid=2790,fd=4),("httpd",pid=2788,fd=4))
```

### 🌞 TEST

vérifier que le service est démarré

vérifier qu'il est configuré pour démarrer automatiquement

```bash
[kaddate@node1 conf]$ sudo systemctl status httpd | egrep "Active: |Loaded: "
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
   Active: active (running) since Wed 2021-09-29 12:16:46 CEST; 16min ago
```

vérifier avec une commande curl localhost que vous joignez votre serveur web localement

```bash
[kaddate@node1 conf]$ curl localhost:80 | wc -l
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  7621  100  7621    0     0  1240k      0 --:--:-- --:--:-- --:--:-- 1240k
308
```

vérifier avec votre navigateur (sur votre PC) que vous accéder à votre serveur web

```bash
➜  tp2 git:(main) ✗ curl 10.102.1.11:80 | wc -l
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  7621  100  7621    0     0  7442k      0 --:--:-- --:--:-- --:--:-- 7442k
308
```

## 2. Avancer vers la maîtrise du service

### 🌞 Le service Apache...

donnez la commande qui permet d'activer le démarrage automatique d'Apache quand la machine s'allume

```bash
sudo systemctl enable httpd
```

prouvez avec une commande qu'actuellement, le service est paramétré pour démarré quand la machine s'allume

```bash
[kaddate@node1 conf]$ sudo systemctl is-enabled httpd
enabled
```

affichez le contenu du fichier httpd.service qui contient la définition du service Apache

```bash
[kaddate@node1 conf]$ cat /usr/lib/systemd/system/httpd.service | grep -E -v '^#'

[Unit]
Description=The Apache HTTP Server
Wants=httpd-init.service
After=network.target remote-fs.target nss-lookup.target httpd-init.service
Documentation=man:httpd.service(8)

[Service]
Type=notify
Environment=LANG=C

ExecStart=/usr/sbin/httpd $OPTIONS -DFOREGROUND
ExecReload=/usr/sbin/httpd $OPTIONS -k graceful
KillSignal=SIGWINCH
KillMode=mixed
PrivateTmp=true

[Install]
WantedBy=multi-user.target
```

### 🌞 Déterminer sous quel utilisateur tourne le processus Apache

mettez en évidence la ligne dans le fichier de conf qui définit quel user est utilisé

```bash
[kaddate@node1 conf]$ cat /etc/httpd/conf/httpd.conf | grep "User "
User apache
```

utilisez la commande ps -ef pour visualiser les processus en cours d'exécution et confirmer que apache tourne bien sous l'utilisateur mentionné dans le fichier de conf

```bash
[kaddate@web ~]$ sudo ps -ef | grep -E "apache|httpd" | egrep -v "grep|root" | awk '{print $1}'
apache
apache
apache
apache
```

vérifiez avec un ls -al le dossier du site (dans /var/www/...) et tout son contenu appartient au même utilisateur

```bash
# je constate que le propriétaire est root, je ne sais pas si c'est normal.
# mais j'ai vu qu'une bonne pratique d'apache consistait à changer le groupe pour 
# un groupe ayant l'utilisateur du service dedans (donc l'utilisateur apache en l'occurence)
[kaddate@web ~]$ ls -larth /var/www
total 4,0K
drwxr-xr-x.  2 root root    6 11 juin  17:35 html
drwxr-xr-x.  2 root root    6 11 juin  17:35 cgi-bin
drwxr-xr-x. 22 root root 4,0K 29 sept. 12:00 ..
drwxr-xr-x.  4 root root   33 29 sept. 12:00 .
```

### 🌞 Changer l'utilisateur utilisé par Apache

créez le nouvel utilisateur

pour les options de création, inspirez-vous de l'utilisateur Apache existant

le fichier /etc/passwd contient les informations relatives aux utilisateurs existants sur la machine
servez-vous en pour voir la config actuelle de l'utilisateur Apache par défaut

```bash
[kaddate@web ~]$ sudo useradd webd --shell /sbin/nologin
```

modifiez la configuration d'Apache pour qu'il utilise ce nouvel utilisateur

```bash
[kaddate@web ~]$ cat /etc/httpd/conf/httpd.conf | grep "User "
User webd
```

redémarrez Apache

```bash
[kaddate@web ~]$ sudo systemctl restart httpd
```

utilisez une commande ps pour vérifier que le changement a pris effet

```bash
[kaddate@web ~]$ ps -ef | egrep "httpd|webd" | grep -v root | grep -v grep
webd        1865    1863  0 14:42 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
webd        1866    1863  0 14:42 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
webd        1867    1863  0 14:42 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
webd        1868    1863  0 14:42 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
```

### 🌞 Faites en sorte que Apache tourne sur un autre port

modifiez la configuration d'Apache pour lui demande d'écouter sur un autre port

```bash
[kaddate@web ~]$ cat /etc/httpd/conf/httpd.conf | grep Listen
Listen 8000
```

ouvrez un nouveau port firewall, et fermez l'ancien

```bash
[kaddate@web ~]$ sudo firewall-cmd --add-port=8000/tcp --permanent
success
[kaddate@web ~]$ sudo firewall-cmd --remove-port=80/tcp --permanent
success
[kaddate@web ~]$ sudo firewall-cmd --reload
success
```

redémarrez Apache

```bash
[kaddate@web ~]$ sudo systemctl restart httpd
```

prouvez avec une commande ss que Apache tourne bien sur le nouveau port choisi

```bash
[kaddate@web ~]$ sudo ss -tanp | grep 8000
LISTEN 0      128                  *:8000             *:*     users:(("httpd",pid=1984,fd=4),("httpd",pid=1983,fd=4),("httpd",pid=1982,fd=4),("httpd",pid=1979,fd=4))
```

vérifiez avec curl en local que vous pouvez joindre Apache sur le nouveau port

```bash
[kaddate@web ~]$ curl localhost:8000 | wc -l
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  7621  100  7621    0     0  1063k      0 --:--:-- --:--:-- --:--:-- 1063k
308
```

vérifiez avec votre navigateur que vous pouvez joindre le serveur sur le nouveau port

```bash
➜  tp3 git:(main) ✗ curl 10.102.1.11:8000 | wc -l
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  7621  100  7621    0     0  7442k      0 --:--:-- --:--:-- --:--:-- 7442k
308
```

## II. Une stack web plus avancée

## 2. Setup

### A. Serveur Web et NextCloud

### 🌞 Install du serveur Web et de NextCloud sur web.tp2.linux

déroulez la doc d'install de Rocky

uniquement pour le serveur Web + NextCloud, vous ferez la base de données MariaDB après.

je veux dans le rendu toutes les commandes réalisées

```bash
# j'ai effacé certaines commandes qui sont des erreurs de copiés collés.
   95  début install
   97  dnf install epel-release
   98  dnf update
   99  dnf install https://rpms.remirepo.net/enterprise/remi-release-8.rpm
  100  dnf module list php
  101  dnf module enable php:remi-7.4
  102  dnf module list php
  103  dnf install httpd mariadb-server vim wget zip unzip libxml2 openssl php74-php php74-php-ctype php74-php-curl php74-php-gd php74-php-iconv php74-php-json php74-php-libxml php74-php-mbstring php74-php-openssl php74-php-posix php74-php-session php74-php-xml php74-php-zip php74-php-zlib php74-php-pdo php74-php-mysqlnd php74-php-intl php74-php-bcmath php74-php-gmp -y
  104  systemctl enable httpd
  105  sudo nano /etc/httpd/sites-available/linux.tp2.web
  107  mkdir /etc/httpd/sites-enabled
  108  mkdir /etc/httpd/sites-available
  110  sudo nano /etc/httpd/sites-available/linux.tp2.web
  111  nano /etc/httpd/conf/httpd.conf 
  116  ln -s /etc/httpd/sites-available/linux.tp2.web /etc/httpd/sites-enabled/
  117  ls /etc/httpd/sites-enabled/
  122  mkdir -p /var/www/sub-domains/linux.tp2.web/html
  126  timedatectl
  128  nano /etc/opt/remi/php74/php.ini
  129  ls -al /etc/localtime
  131  wget https://download.nextcloud.com/server/releases/nextcloud-21.0.1.zip
  132  unzip nextcloud-21.0.1.zip 
  133  cd nextcloud
  134  cp -Rf * /var/www/sub-domains/linux.tp2.web/html/
  137  chown -Rf apache.apache /var/www/sub-domains/linux.tp2.web/html
  139  ls /var/www/sub-domains/linux.tp2.web/ -larth
  141  systemctl restart httpd
  142  history
```

### B. Base de données

### 🌞 Install de MariaDB sur db.tp2.linux

déroulez la doc d'install de Rocky

manipulation

je veux dans le rendu toutes les commandes réalisées

```bash
   31  dnf install mariadb-server
   32  sudo dnf install mariadb-server
   33  sudo systemctl start mariadb
   34  sudo mysql_secure_installation
   35  history
```

vous repérerez le port utilisé par MariaDB avec une commande ss exécutée sur db.tp2.linux

```bash
[kaddate@db etc]$ sudo ss -tanp | grep 3306
LISTEN 0      80                 *:3306            *:*     users:(("mysqld",pid=38004,fd=21))
```

### 🌞 Préparation de la base pour NextCloud

une fois en place, il va falloir préparer une base de données pour NextCloud :

connectez-vous à la base de données à l'aide de la commande sudo mysql -u root -p

exécutez les commandes SQL suivantes :

```bash
[kaddate@db etc]$ mysql -u root -p
Enter password: 
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 15
Server version: 10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> 
MariaDB [(none)]> 
MariaDB [(none)]> # Création d'un utilisateur dans la base, avec un mot de passe
MariaDB [(none)]> # L'adresse IP correspond à l'adresse IP depuis laquelle viendra les connexions. Cela permet de restreindre les IPs autorisées à se connecter.
MariaDB [(none)]> # Dans notre cas, c'est l'IP de web.tp2.linux
MariaDB [(none)]> # "meow" c'est le mot de passe :D
MariaDB [(none)]> CREATE USER 'nextcloud'@'10.102.1.11' IDENTIFIED BY 'meow';
Query OK, 0 rows affected (0.001 sec)

MariaDB [(none)]> 
MariaDB [(none)]> # Création de la base de donnée qui sera utilisée par NextCloud
MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
Query OK, 1 row affected (0.001 sec)

MariaDB [(none)]> 
MariaDB [(none)]> # On donne tous les droits à l'utilisateur nextcloud sur toutes les tables de la base qu'on vient de créer
MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.102.1.11';
Query OK, 0 rows affected (0.000 sec)

MariaDB [(none)]> 
MariaDB [(none)]> # Actualisation des privilèges
MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.000 sec)

MariaDB [(none)]> 
```

### 🌞 Exploration de la base de données

afin de tester le bon fonctionnement de la base de données, vous allez essayer de vous connecter, comme NextCloud le fera :

depuis la machine web.tp2.linux vers l'IP de db.tp2.linux

vous pouvez utiliser la commande mysql pour vous connecter à une base de données depuis la ligne de commande

```bash
[kaddate@web ~]$ mysql -u nextcloud -h 10.102.1.12 -pmeow
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 17
Server version: 10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> 
```

utilisez les commandes SQL fournies ci-dessous pour explorer la base

```bash
MariaDB [(none)]> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| nextcloud          |
+--------------------+
2 rows in set (0.005 sec)

MariaDB [(none)]> use nextcloud;
Database changed
MariaDB [nextcloud]> show tables;
Empty set (0.002 sec)
```

trouver une commande qui permet de lister tous les utilisateurs de la base de données

```bash
# voici la commande que j'utilise, mais l'utilisateur Nextcloud n'a pas les droits pour aller dans des bases de données autres que "nextcloud"
SELECT user FROM mysql.user;
```

## C. Finaliser l'installation de NextCloud

### 🌞 sur votre PC

modifiez votre fichier hosts (oui, celui de votre PC, de votre hôte)

pour pouvoir joindre l'IP de la VM en utilisant le nom web.tp2.linux

```bash
➜  tp2 git:(main) ✗ echo "10.102.1.11 web.tp2.linux" | sudo tee -a /etc/hosts
10.102.1.11 web.tp2.linux
```

### 🌞 Exploration de la base de données

connectez vous en ligne de commande à la base de données après l'installation terminée

déterminer combien de tables ont été crées par NextCloud lors de la finalisation de l'installation

bonus points si la réponse à cette question est automatiquement donnée par une requête SQL

```bash
# l'utilisateur NextCloud a créé 77 tables
MariaDB [(none)]> SELECT COUNT(*) from information_schema.tables where TABLE_SCHEMA = 'nextcloud';
+----------+
| COUNT(*) |
+----------+
|       77 |
+----------+
1 row in set (0.002 sec)
```

| Machine         | IP            | Service                 | Port ouvert | IP autorisées |
|-----------------|---------------|-------------------------|-------------|---------------|
| `web.tp2.linux` | `10.102.1.11` | Serveur Web             | 80        | x             |
| `db.tp2.linux` | `10.102.1.12` | Serveur BDD             | 3306        | 10.102.1.11             |

> Ce tableau devra figurer à la fin du rendu, avec les ? remplacés par la bonne valeur (un seul tableau à la fin). Je vous le remets à chaque fois, à des fins de clarté, pour lister les machines qu'on a à chaque instant du TP.