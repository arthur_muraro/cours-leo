# TP2 pt. 2 : Maintien en condition opérationnelle

## I. Monitoring

### 2. Setup

### 🌞 Setup Netdata

y'a plein de méthodes d'install pour Netdata
on va aller au plus simple, exécutez, sur toutes les machines que vous souhaitez monitorer :

```bash
[root@web ~]# ls -larth /opt/netdata/etc/netdata/netdata.conf 
-rw-r--r--. 1 root root 226K Oct 11 11:32 /opt/netdata/etc/netdata/netdata.conf
```

```bash
[root@db ~]# ls -larth /opt/netdata/etc/netdata/netdata.conf
-rw-r--r--. 1 root root 214K Oct 11 11:32 /opt/netdata/etc/netdata/netdata.conf
```

### 🌞 Manipulation du service Netdata

```bash
# toutes les manipulations ci dessous seront faites sur le serveur WEB, mais les manipulations sont faites sur les deux.
[kaddate@web ~]$ sudo systemctl is-enabled netdata
enabled
[kaddate@web ~]$ sudo ss -tanp | grep netdata | egrep -v "::|127" | awk -F':' '{print $2}' | cut -d' ' -f1
19999
[kaddate@web ~]$ sudo firewall-cmd --add-port=19999/tcp --permanent
success
[kaddate@web ~]$ sudo firewall-cmd --reload
success
```

### 🌞 Setup Alerting

ajustez la conf de Netdata pour mettre en place des alertes Discord

ui ui c'est bien ça : vous recevrez un message Discord quand un seul critique est atteint

c'est là que ça se passe dans la doc de Netdata
vérifiez le bon fonctionnement de l'alerting sur Discord

```bash
[kaddate@web ~]$ cat /opt/netdata/etc/netdata/health_alarm_notify.conf | grep DISCORD_WEBHOOK
DISCORD_WEBHOOK_URL="https://discord.com/api/webhooks/897030631324794920/iuQzM_aEHkCzjYoxP89PCZSxoMiiDg_eJwh3Zb2koxb5Ad1PBWe8DvYMAK1iC2DH6EJu"
[kaddate@web netdata]$ /opt/netdata/usr/libexec/netdata/plugins.d/alarm-notify.sh test

# SENDING TEST WARNING ALARM TO ROLE: sysadmin
2021-10-11 12:59:38: alarm-notify.sh: INFO: sent discord notification for: web.tp2.linux test.chart.test_alarm is WARNING to 'netdata'
# OK

# SENDING TEST CRITICAL ALARM TO ROLE: sysadmin
2021-10-11 12:59:39: alarm-notify.sh: INFO: sent discord notification for: web.tp2.linux test.chart.test_alarm is CRITICAL to 'netdata'
# OK

# SENDING TEST CLEAR ALARM TO ROLE: sysadmin
2021-10-11 12:59:39: alarm-notify.sh: INFO: sent discord notification for: web.tp2.linux test.chart.test_alarm is CLEAR to 'netdata'
# OK
```

### 🌞 Config alerting

créez une nouvelle alerte pour recevoir une alerte à 50% de remplissage de la RAM
testez que votre alerte fonctionne

il faudra remplir artificiellement la RAM pour voir si l'alerte remonte correctement
sur Linux, on utilise la commande stress pour ça

```bash
[kaddate@web netdata]$ cat health.d/ram_50.conf 
 alarm: ram_usage
    on: system.ram
lookup: average -1m percentage of used
 units: %
 every: 1m
  warn: $this > 50
  crit: $this > 80
  info: The percentage of RAM being used by the system.
# commande de stress
  [kaddate@web netdata]$ stress -m 1 --vm-bytes 800M --vm-keep
stress: info: [2751] dispatching hogs: 0 cpu, 0 io, 1 vm, 0 hdd
```

## II. Backup

### 2. Partage NFS

### 🌞 Setup environnement

créer un dossier /srv/backup/

il contiendra un sous-dossier pour chaque machine du parc

```bash
[kaddate@backup ~]$ sudo mkdir -p /srv/backup/web.tp2.linux/
[kaddate@backup ~]$ sudo mkdir /srv/backup/db.tp2.linux/
```

### 🌞 Setup partage NFS

je crois que vous commencez à connaître la chanson... Google "nfs server rocky linux"

ce lien me semble être particulièrement simple et concis

```bash
   20  sudo dnf -y install nfs-utils
   21  sudo vi /etc/idmapd.conf 
   22  sudo vi /etc/exports 
   23  sudo systemctl enable --now rpcbind nfs-server 
   24  sudo firewall-cmd --add-service=nfs 
   25  sudo firewall-cmd --add-service=nfs --permanent
   26  sudo firewall-cmd --reload
   27  sudo firewall-cmd --list-all
   28  history
```

### 🌞 Setup points de montage sur web.tp2.linux

sur le même site, y'a ça
monter le dossier /srv/backups/web.tp2.linux du serveur NFS dans le dossier /srv/backup/ du serveur Web
vérifier...

faites en sorte que cette partition se monte automatiquement grâce au fichier /etc/fstab

```bash
[kaddate@web ~]$ cat /etc/fstab | grep backup
backup.tp2.linux:/srv/backup/web.tp2.linux /srv/backup               nfs     defaults        0 0
[kaddate@web ~]$ df -h | grep backup
backup.tp2.linux:/srv/backup/web.tp2.linux   6,2G    2,1G  4,2G  33% /srv/backup
```

### 🌟 BONUS : partitionnement avec LVM

ajoutez un disque à la VM backup.tp2.linux

utilisez LVM pour créer une nouvelle partition (5Go ça ira)

monter automatiquement cette partition au démarrage du système à l'aide du fichier /etc/fstab

cette nouvelle partition devra être montée sur le dossier /srv/backup/

```bash
[kaddate@backup ~]$ sudo lsblk | grep sdb
sdb           8:16   0  5G  0 disk
[kaddate@backup ~]$ sudo fdisk /dev/sdb
# je vire l'Affichage
[kaddate@backup ~]$ sudo lsblk | grep sdb
sdb           8:16   0  5G  0 disk 
└─sdb1        8:17   0  5G  0 part
[kaddate@backup ~]$ sudo echo "/dev/sdb1 /srv/backup ext4 defaults 0 0" | sudo tee -a /etc/fstab
/dev/sdb1 /srv/backup ext4 defaults 0 0
```

## 3. Backup de fichiers

### 🌞 Rédiger le script de backup /srv/tp2_backup.sh

le script crée une archive compressée .tar.gz du dossier ciblé

l'archive générée doit s'appeler tp2_backup_YYMMDD_HHMMSS.tar.gz

le script utilise la commande rsync afin d'envoyer la sauvegarde dans le dossier de destination

```bash
#!/bin/bash
# Simple backup script
# usage : backup.sh <destination> <dir_to_backup>
# Kaddate - 12/10/2021

_archive_name=$(date +tp2_backup_%F_%H.%M.%S.tar.gz)
tar czf $_archive_name $2
rsync -a --remove-source-files $_archive_name $1
```

### 🌞 Tester le bon fonctionnement

exécuter le script sur le dossier de votre choix
prouvez que la backup s'est bien exécutée

tester de restaurer les données

récupérer l'archive générée, et vérifier son contenu

```bash
➜  tp2-2 git:(main) ✗ ls testdir                                            
testfile
➜  tp2-2 git:(main) ✗ ./backup.sh .. testdir 
➜  tp2-2 git:(main) ✗ cd ..
➜  linux git:(main) ✗ ls
tp1  tp2  tp2-2  tp2_backup_2021-10-19_21.12.03.tar.gz
➜  linux git:(main) ✗ tar -zxvf tp2_backup_2021-10-19_21.12.03.tar.gz 
testdir/
testdir/testfile
➜  linux git:(main) ✗ ls
testdir  tp1  tp2  tp2-2  tp2_backup_2021-10-19_21.12.03.tar.gz
➜  linux git:(main) ✗ ls testdir 
testfile
```

## 4. Unité de service

### 🌞 Créer une unité de service pour notre backup

c'est juste un fichier texte hein
doit se trouver dans le dossier /etc/systemd/system/

doit s'appeler tp2_backup.service

```bash
[kaddate@web ~]$ cat /etc/systemd/system/tp2_backup.service 
[Unit]
Description=Our own lil backup service (TP2)

[Service]
ExecStart=/srv/tp2_backup.sh /srv/backup/ /var/www/sub-domains/linux.tp2.web/
Type=oneshot
RemainAfterExit=no

[Install]
WantedBy=multi-user.target
```

### 🌞 Tester le bon fonctionnement

**pour la création du service j'ai prit le script que tu as fait, ce sera plus simple si il y'a des erreurs plus tard dans le TP**

essayez d'effectuer une sauvegarde avec sudo systemctl start backup

vérifiez la présence de la nouvelle archive

```bash
[kaddate@web ~]$ sudo systemctl daemon-reload
[kaddate@web ~]$ sudo systemctl start tp2_backup.service 

[kaddate@web ~]$ 
# partie serveur nfs
[kaddate@backup web.tp2.linux]$ ls
hello_211025_183020.tar.gz
```

## B. Timer

### 🌞 Créer le timer associé à notre tp2_backup.service

```bash
[kaddate@web ~]$ cat /etc/systemd/system/tp2_backup.timer 
[Unit]
Description=Periodically run our TP2 backup script
Requires=tp2_backup.service

[Timer]
Unit=tp2_backup.service
OnCalendar=*-*-* *:*:00

[Install]
WantedBy=timers.target
```

### 🌞 Activez le timer

prouver que 
le timer est actif actuellement
qu'il est paramétré pour être actif dès que le système boot

```bash
[kaddate@web ~]$ sudo systemctl start tp2_backup.timer
[kaddate@web ~]$ sudo systemctl enable tp2_backup.timer
[kaddate@web ~]$ sudo systemctl status tp2_backup.timer | sed -n '2,3p' | cut -d' ' -f5
loaded
active
```

### 🌞 Tests !

avec la ligne OnCalendar=*-*-* *:*:00, le timer déclenche l'exécution du service toutes les minutes
vérifiez que la backup s'exécute correctement

```bash
# coté serveur nfs
[kaddate@backup web.tp2.linux]$ ls && sleep 61 && ls
hello_211025_183020.tar.gz  hello_211025_183621.tar.gz  hello_211025_183700.tar.gz  hello_211025_183800.tar.gz  hello_211025_183900.tar.gz
hello_211025_183621.tar.gz  hello_211025_183700.tar.gz  hello_211025_183800.tar.gz  hello_211025_183900.tar.gz  hello_211025_184000.tar.gz
```

## C. Contexte

### 🌞 Faites en sorte que...

votre backup s'exécute sur la machine web.tp2.linux

```bash
[kaddate@web ~]$ ls /etc/systemd/system/tp2_backup.service && cat /etc/hostname
/etc/systemd/system/tp2_backup.service
web.tp2.linux
```

le dossier sauvegardé est celui qui contient le site NextCloud (quelque part dans /var/)

```bash
[kaddate@web ~]$ cat /etc/systemd/system/tp2_backup.service | grep www
ExecStart=/srv/backup.sh /srv/backup/ /var/www/sub-domains/linux.tp2.web/
```

la destination est le dossier NFS monté depuis le serveur backup.tp2.linux

```bash
[kaddate@web ~]$ cat /etc/systemd/system/tp2_backup.service | grep /srv/backup/ && cat /etc/fstab | grep backup.tp2.linux
ExecStart=/srv/backup.sh /srv/backup/ /var/www/sub-domains/linux.tp2.web/
backup.tp2.linux:/srv/backup/web.tp2.linux /srv/backup               nfs     defaults        0 0
```

la sauvegarde s'exécute tous les jours à 03h15 du matin
prouvez avec la commande sudo systemctl list-timers que votre service va bien s'exécuter la prochaine fois qu'il sera 03h15

```bash
[kaddate@web system]$ cat tp2_backup.timer | grep OnCalendar
OnCalendar=*-*-* 03:15:00
```

```bash

```

```bash

```

```bash

```

```bash

```

```bash

```

```bash

```

```bash

```
