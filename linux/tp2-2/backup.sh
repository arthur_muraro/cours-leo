#!/bin/bash
# Simple backup script
# usage : backup.sh <destination> <dir_to_backup>
# Kaddate - 12/10/2021

_archive_name=$(date +tp2_backup_%F_%H.%M.%S.tar.gz)

tar czf $_archive_name $2

rsync -a --remove-source-files $_archive_name $1