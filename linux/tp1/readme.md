# TP1 : (re)Familiaration avec un système GNU/Linux

## 0. Préparation de la machine

### 🌞 Setup de deux machines Rocky Linux configurée de façon basique.

**un accès internet (via la carte NAT)**

```bash
[kaddate@node1 ~]$ ping 8.8.8.8 -c2 | grep "packet loss"
2 packets transmitted, 2 received, 0% packet loss, time 1002ms

[kaddate@node2 ~]$ ping 8.8.8.8 -c2 | grep "packet loss"
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
```

**un accès à un réseau local** (les deux machines peuvent se ping) (via la carte Host-Only)

```bash
[kaddate@node1 ~]$ ping 10.101.1.12 -c2
PING 10.101.1.12 (10.101.1.12) 56(84) bytes of data.
64 bytes from 10.101.1.12: icmp_seq=1 ttl=64 time=0.758 ms
64 bytes from 10.101.1.12: icmp_seq=2 ttl=64 time=0.787 ms

--- 10.101.1.12 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1021ms
rtt min/avg/max/mdev = 0.758/0.772/0.787/0.031 ms
```

**les machines doivent avoir un nom**

```bash
[kaddate@node1 ~]$ hostname
node1.tp1.b2

[kaddate@node2 ~]$ hostname
node2.tp1.b2
```

**utiliser 1.1.1.1 comme serveur DNS**

```bash
[kaddate@node1 ~]$ sudo sed -i '2 i nameserver 1.1.1.1' /etc/resolv.conf

[kaddate@node2 ~]$ sudo sed -i '2 i nameserver 1.1.1.1' /etc/resolv.conf
```

**vérifier avec le bon fonctionnement avec la commande dig**

```bash
# addresse ip correspondante à ynov.com
[kaddate@node1 ~]$ dig ynov.com | sed -n '13,14p'
;; ANSWER SECTION:
ynov.com.		10408	IN	A	92.243.16.143

# server ayant répondu.
[kaddate@node1 ~]$ dig ynov.com | grep SERVER
;; SERVER: 1.1.1.1#53(1.1.1.1)
```

**les machines doivent pouvoir se joindre par leurs noms respectifs**

```bash
[kaddate@node1 ~]$ sudo echo "10.101.1.12 node2" | sudo tee -a /etc/hosts
10.101.1.12 node2

[kaddate@node2 ~]$ sudo echo "10.101.1.11 node1" | sudo tee -a /etc/hosts
10.101.1.11 node1

# vérification
[kaddate@node2 ~]$ ping node1 -c2 | grep "packet loss"
2 packets transmitted, 2 received, 0% packet loss, time 1019ms
```

**le pare-feu est configuré pour bloquer toutes les connexions exceptées celles qui sont nécessaires**

```bash
[kaddate@node1 ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: ens33 ens36
  sources: 
  services: cockpit dhcpv6-client ssh
  ports: 
  protocols: 
  masquerade: no
  forward-ports: 
  source-ports: 
  icmp-blocks: 
  rich rules: 

[kaddate@node2 ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: ens33 ens36 ens37
  sources: 
  services: cockpit dhcpv6-client ssh
  ports: 
  protocols: 
  masquerade: no
  forward-ports: 
  source-ports: 
  icmp-blocks: 
  rich rules: 
```

## I. Utilisateurs

### 1. Création et configuration

### 🌞 Ajouter un utilisateur à la machine, qui sera dédié à son administration. Précisez des options sur la commande d'ajout pour que

```bash
[kaddate@node1 ~]$ sudo useradd admin --home /home/admin/ --create-home --shell /bin/bash
# vérification
[kaddate@node1 ~]$ cat /etc/passwd | grep admin
admin:x:1001:1001::/home/admin/:/bin/bash

[kaddate@node2 ~]$ sudo useradd admin --home /home/admin/ --create-home --shell /bin/bash
# vérification
[kaddate@node2 ~]$ cat /etc/passwd | grep admin
admin:x:1001:1001::/home/admin/:/bin/bash
```

### 🌞 Créer un nouveau groupe admins qui contiendra les utilisateurs de la machine ayant accès aux droits de root via la commande sudo

```bash
[kaddate@node2 ~]$ sudo groupadd admins

[kaddate@node1 ~]$ sudo groupadd admins
```

```bash
[kaddate@node1 ~]$ sudo visudo
[kaddate@node1 ~]$ sudo cat /etc/sudoers | grep admins
%admins ALL=(ALL)	ALL

[kaddate@node2 ~]$ sudo visudo
[kaddate@node2 ~]$ sudo cat /etc/sudoers | grep admins
%admins ALL=(ALL)	ALL
```

### 🌞 Ajouter votre utilisateur à ce groupe admins

```bash
[kaddate@node1 ~]$ sudo usermod -a -G admins kaddate

[kaddate@node2 ~]$ sudo usermod -a -G admins kaddate
```

### 2. SSH

### 🌞 Afin de se connecter à la machine de façon plus sécurisée, on va configurer un échange de clés SSH lorsque l'on se connecte à la machine. Pour cela

il faut générer une clé sur le poste client de l'administrateur qui se connectera à distance (vous :) )

```bash
# commande pour générer une paire de clef ssh
➜  tp1 git:(main) ✗ ssh-keygen -t rsa
```

déposer la clé dans le fichier /home/<USER>/.ssh/authorized_keys de la machine que l'on souhaite administrer

```bash
➜  tp1 git:(main) ✗ ssh-copy-id -i ~/.ssh/id_rsa2.pub kaddate@192.168.138.132 
/usr/bin/ssh-copy-id: INFO: Source of key(s) to be installed: "/home/kaddate/.ssh/id_rsa2.pub"
/usr/bin/ssh-copy-id: INFO: attempting to log in with the new key(s), to filter out any that are already installed
/usr/bin/ssh-copy-id: INFO: 1 key(s) remain to be installed -- if you are prompted now it is to install the new keys
kaddate@192.168.138.132's password: 

Number of key(s) added: 1

Now try logging into the machine, with:   "ssh 'kaddate@192.168.138.132'"
and check to make sure that only the key(s) you wanted were added.
```

### 🌞 Assurez vous que la connexion SSH est fonctionnelle, sans avoir besoin de mot de passe

```bash
➜  ~ ssh 192.168.138.132 
Activate the web console with: systemctl enable --now cockpit.socket

Last login: Wed Sep 22 17:03:41 2021 from 192.168.138.1
[kaddate@node1 ~]$
```

## II. Partitionnement

### 2. Partitionnement

```bash
# procédure :
# detection des disque :
[kaddate@node1 ~]$ sudo lsblk | egrep "sdb|sdc"
sdb           8:16   0    3G  0 disk 
sdc           8:32   0    3G  0 disk 

# création d'une partition pour chaques disques :
[kaddate@node1 ~]$ sudo fdisk /dev/sdb
[kaddate@node1 ~]$ sudo fdisk /dev/sdc

[kaddate@node1 ~]$ sudo lsblk | egrep "sdb|sdc"
sdb           8:16   0    3G  0 disk 
└─sdb1        8:17   0    3G  0 part 
sdc           8:32   0    3G  0 disk 
└─sdc1        8:33   0    3G  0 part 

# préparer les disques à l'utilisation de LVM
[kaddate@node1 ~]$ sudo pvcreate /dev/sdb1
  Physical volume "/dev/sdb1" successfully created.
[kaddate@node1 ~]$ sudo pvcreate /dev/sdc1
  Physical volume "/dev/sdc1" successfully created.

# création du groupe contenant mes deux disques
[kaddate@node1 ~]$ sudo vgcreate groupdisk /dev/sdb1 /dev/sdc1
  Volume group "groupdisk" successfully created

# vérification de la bonne création du groupe
[kaddate@node1 dev]$ sudo pvs | grep groupdisk
  /dev/sdb1  groupdisk lvm2 a--  <3,00g <3,00g
  /dev/sdc1  groupdisk lvm2 a--  <3,00g <3,00g

# création des volumes logiques
[kaddate@node1 dev]$ sudo lvcreate -n lv1 -L 1024M groupdisk
  Logical volume "lv1" created.
[kaddate@node1 dev]$ sudo lvcreate -n lv2 -L 1024M groupdisk
  Logical volume "lv2" created.
[kaddate@node1 dev]$ sudo lvcreate -n lv3 -L 1024M groupdisk
  Logical volume "lv3" created.

# vérifications de la bonne création des volumes logiques
[kaddate@node1 dev]$ lsblk | grep "lv[1-3]"
  ├─groupdisk-lv1 253:2    0    1G  0 lvm  
  └─groupdisk-lv2 253:3    0    1G  0 lvm  
  └─groupdisk-lv3 253:4    0    1G  0 lvm

# formattage des disques
[kaddate@node1 groupdisk]$ mkfs.ext4 /dev/groupadd/lv1
[kaddate@node1 groupdisk]$ mkfs.ext4 /dev/groupadd/lv2
[kaddate@node1 groupdisk]$ mkfs.ext4 /dev/groupadd/lv3

# montage permanent de ceux-ci
[kaddate@node1 groupdisk]$ sudo mkdir -p /mnt/{part1,part2,part3}
[kaddate@node1 groupdisk]$ sudo echo "/dev/groupdisk/lv1 /mnt/part1 ext4 defaults 0 0" | sudo tee -a /etc/fstab
/dev/groupdisk/lv1 /mnt/part1 ext4 defaults 0 0
[kaddate@node1 groupdisk]$ sudo echo "/dev/groupdisk/lv2 /mnt/part2 ext4 defaults 0 0" | sudo tee -a /etc/fstab
/dev/groupdisk/lv2 /mnt/part2 ext4 defaults 0 0
[kaddate@node1 groupdisk]$ sudo echo "/dev/groupdisk/lv3 /mnt/part3 ext4 defaults 0 0" | sudo tee -a /etc/fstab
/dev/groupdisk/lv3 /mnt/part3 ext4 defaults 0 0

# vérification finale
[kaddate@node1 ~]$ cd /mnt/part1
[kaddate@node1 part1]$ sudo touch something
[kaddate@node1 part1]$ ls
lost+found  something
```

## III. Gestion de services

## 1. Interaction avec un service existant

### 🌞 Assurez-vous que firewalld

est démarrée

```bash
[kaddate@node1 part1]$ sudo systemctl status firewalld --no-pager | grep "Active: "
   Active: active (running) since Fri 2021-09-24 20:23:43 CEST; 8min ago
```

est activée (elle se lance automatiquement au démarrage)

```bash
[kaddate@node1 part1]$ sudo systemctl status firewalld --no-pager | grep "enabled;"
   Loaded: loaded (/usr/lib/systemd/system/firewalld.service; enabled; vendor preset: enabled)
```

## 2. Création de service

## A. Unité simpliste

### 🌞 Créer un fichier qui définit une unité de service web.service dans le répertoire /etc/systemd/system

```bash
echo "[Unit]
Description=Very simple web service

[Service]
ExecStart=/bin/python3 -m http.server 8888

[Install]
WantedBy=multi-user.target" | sudo tee /etc/systemd/system/web.service
```

Une fois l'unité de service créée, il faut demander à systemd de relire les fichiers de configuration :

Enfin, on peut interagir avec notre unité :

```bash
[kaddate@node1 part1]$ sudo systemctl daemon-reload
[kaddate@node1 part1]$ sudo systemctl status web --no-pager
● web.service - Very simple web service
   Loaded: loaded (/etc/systemd/system/web.service; disabled; vendor preset: disabled)
   Active: inactive (dead)
[kaddate@node1 part1]$ sudo systemctl start web
[kaddate@node1 part1]$ sudo systemctl enable web
Created symlink /etc/systemd/system/multi-user.target.wants/web.service → /etc/systemd/system/web.service.
```

### 🌞 Une fois le service démarré, assurez-vous que pouvez accéder au serveur web : avec un navigateur ou la commande curl sur l'IP de la VM, port 8888

```bash
[kaddate@node1 part1]$ curl localhost:8888
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>

[...]

<li><a href="var/">var/</a></li>
</ul>
<hr>
</body>
</html>
```

## B. Modification de l'unité

### 🌞 Créer un utilisateur web

```bash
[kaddate@node1 part1]$ sudo useradd web
```

### 🌞 Modifiez l'unité de service web.service créée précédemment en ajoutant les clauses

User= afin de lancer le serveur avec l'utilisateur web dédié

WorkingDirectory= afin de lancer le serveur depuis un dossier spécifique, choisissez un dossier que vous avez créé dans /srv

ces deux clauses sont à positionner dans la section [Service] de votre unité

```bash
# <(￣ c￣)y▂ξ
[kaddate@node1 part1]$ sudo awk '/Service/ { print; print "User=web\nWorkingDirectory=/srv/webservice"; next }1' /etc/systemd/system/web.service | sudo tee /etc/systemd/system/web.service 
```

### 🌞 Placer un fichier de votre choix dans le dossier créé dans /srv et tester que vous pouvez y accéder une fois le service actif. Il faudra que le dossier et le fichier qu'il contient appartiennent à l'utilisateur web

```bash
# création du fichier
[kaddate@node1 part1]$ sudo touch /srv/webservice/fichier_servi
[kaddate@node1 part1]$ sudo ls /srv/webservice/
fichier_servi

# gestion des propriétaires sur le dossier
[kaddate@node1 srv]$ sudo chown -R web:web webservice/
[kaddate@node1 srv]$ ls -larth webservice/
total 0
drwxr-xr-x. 3 root root 24 24 sept. 20:57 ..
-rw-r--r--. 1 web  web   0 24 sept. 21:12 fichier_servi
drwxr-xr-x. 2 web  web  27 24 sept. 21:12 .

# restart du service avec la nouvelle conf
[kaddate@node1 part1]$ sudo systemctl daemon-reload
[kaddate@node1 part1]$ sudo systemctl restart web.service 
```

### 🌞 Vérifier le bon fonctionnement avec une commande curl

```bash
[kaddate@node1 srv]$ curl localhost:8888
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Directory listing for /</title>
</head>
<body>
<h1>Directory listing for /</h1>
<hr>
<ul>
<li><a href="fichier_servi">fichier_servi</a></li>
</ul>
<hr>
</body>
</html>
```
