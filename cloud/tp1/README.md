# TP1 Prise en main de Docker

## I. Prise en main

## 1. Lancer des conteneurs

### 🌞 lister les conteneurs actifs, et mettre en évidence le conteneur lancé sur le sleep

```bash
arthur@ubuntu:~$ docker ps | grep sleep
2030e41afdbc   alpine    "sleep 9999"   4 minutes ago   Up 4 minutes             infallible_jackson
```

### on peut "rentrer" dans un conteneur. Concrètement, ça signifie

```bash
arthur@ubuntu:~$ docker exec -it infallible_jackson sh
/ # cd /etc/
/etc # echo test
test
/etc # 
```

### 🌞 Mise en évidence d'une partie de l'isolation mise en place par le conteneur. Montrer que le conteneur utilise

une arborescence de processus différente

```bash
arthur@ubuntu:~$ docker exec -it infallible_jackson sh
/ # ps 
PID   USER     TIME  COMMAND
    1 root      0:00 sleep 9999
   23 root      0:00 sh
   28 root      0:00 ps
```

des cartes réseau différentes

```bash
/ # ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
7: eth0@if8: <BROADCAST,MULTICAST,UP,LOWER_UP,M-DOWN> mtu 1500 qdisc noqueue state UP 
    link/ether 02:42:ac:11:00:02 brd ff:ff:ff:ff:ff:ff
    inet 172.17.0.2/16 brd 172.17.255.255 scope global eth0
       valid_lft forever preferred_lft forever
```

des utilisateurs système différents

```bash
/ # cat /etc/passwd
root:x:0:0:root:/root:/bin/ash
bin:x:1:1:bin:/bin:/sbin/nologin
[...]
guest:x:405:100:guest:/dev/null:/sbin/nologin
nobody:x:65534:65534:nobody:/:/sbin/nologin
```

des points de montage (les partitions montées) différents

```bash
/ # df -h
Filesystem                Size      Used Available Use% Mounted on
overlay                  38.6G     15.2G     21.4G  42% /
tmpfs                    64.0M         0     64.0M   0% /dev
tmpfs                     2.9G         0      2.9G   0% /sys/fs/cgroup
shm                      64.0M         0     64.0M   0% /dev/shm
/dev/sda5                38.6G     15.2G     21.4G  42% /etc/resolv.conf
[...]
```

### 🌞 détruire le conteneur avec docker rm

```bash
arthur@ubuntu:~$ docker rm infallible_jackson 
infallible_jackson
```

### 🌞 Lancer un conteneur NGINX

```bash
arthur@ubuntu:~$ docker run -d -p 8080:80 nginx
06defc98dec3890e85508694dad21743581c432ba725a492947ba2e08b841277
```

![si cette image ne s'affiche pas, le fichier est présent dans le meme dossier que le markdown au nom de "nginx.png"](./nginx.png?raw=true)

## 2. Gestion d'images

## Gestion d'images

### 🌞 récupérer une image de Apache en version 2.2

```bash
arthur@ubuntu:~/cours-leo/cloud/tp1$ docker pull httpd:2.2.29
2.2.29: Pulling from library/httpd
Image docker.io/library/httpd:2.2.29 uses outdated schema1 manifest format. Please upgrade to a schema2 image for better future compatibility. More information at https://docs.docker.com/registry/spec/deprecated-schema-v1/
[...]
Digest: sha256:0a39699d267aaee04382c6b1b4fe2fc30737450fe8d4fabd88eee1a3e0016144
Status: Downloaded newer image for httpd:2.2.29
docker.io/library/httpd:2.2.29
```

la lancer en partageant un port qui permet d'accéder à la page d'accueil d'Apache

```bash
arthur@ubuntu:~/cours-leo/cloud/tp1$ docker run -p 8080:80 -d httpd:2.2.29
2bfa8aea00f6c8ee4a795255abea2067c71a6a3ed6ba94eefd057094fda822b4
```

![si cette image ne s'affiche pas, le fichier est présent dans le meme dossier que le markdown au nom de "apache.png"](./apache.png?raw=true)

## Création d'image

### 🌞 créer une image qui lance un serveur web python. L'image doit

contenu du dockerfile

```bash
arthur@ubuntu:~/cours-leo/cloud/tp1$ cat Dockerfile 
# Image de base
FROM alpine

# Ajout d'une application dans le conteneur
RUN apk update && apk add python3

# expose
EXPOSE 8888/tcp

WORKDIR .

COPY README.md .

# Définition d'un processus à lancer lorsque le conteneur démarre
CMD python3 -m http.server 8888
```

création de l'image

```bash
arthur@ubuntu:~/cours-leo/cloud/tp1$ docker build -t pythonserver .
Sending build context to Docker daemon  57.86kB
Step 1/6 : FROM alpine
 ---> 14119a10abf4
Step 2/6 : RUN apk update && apk add python3
 ---> Running in 6849c6078aa8
fetch https://dl-cdn.alpinelinux.org/alpine/v3.14/main/x86_64/APKINDEX.tar.gz
fetch https://dl-cdn.alpinelinux.org/alpine/v3.14/community/x86_64/APKINDEX.tar.gz
v3.14.2-40-gf566f645a9 [https://dl-cdn.alpinelinux.org/alpine/v3.14/main]
v3.14.2-42-gf168ad374f [https://dl-cdn.alpinelinux.org/alpine/v3.14/community]
OK: 14938 distinct packages available
(1/13) Installing libbz2 (1.0.8-r1)
[...]
(13/13) Installing python3 (3.9.5-r1)
Executing busybox-1.33.1-r3.trigger
OK: 56 MiB in 27 packages
Removing intermediate container 6849c6078aa8
 ---> 5dcdac894a71
Step 3/6 : EXPOSE 80/tcp
 ---> Running in e104009e9373
Removing intermediate container e104009e9373
 ---> 3e25e1e6b856
Step 4/6 : WORKDIR .
 ---> Running in 7c2ec0aeacdf
Removing intermediate container 7c2ec0aeacdf
 ---> 68a9bd11fdb2
Step 5/6 : COPY README.md .
 ---> 1c9655f47756
Step 6/6 : CMD python3 -m http.server 8888
 ---> Running in c86b4f17ced5
Removing intermediate container c86b4f17ced5
 ---> 27b8438ca7ed
Successfully built 27b8438ca7ed
Successfully tagged pythonserver:latest
```

### 🌞 lancer le conteneur et accéder au serveur web du conteneur depuis votre PC

```bash
arthur@ubuntu:~/cours-leo/cloud/tp1$ docker run -d -p 8080:8888 pythonserver
346fe31b3ce86a1308b2a45ccc4cf0136b815c265b2f54ef87b6d2a01699fe19
```

![si cette image ne s'affiche pas, le fichier est présent dans le meme dossier que le markdown au nom de "pythonserver.png"](./pythonserver.png?raw=true)

### 🌞 utiliser l'option -v de docker run

```bash
arthur@ubuntu:~/cours-leo/cloud/tp1$ docker run -d -v /test/:/ -p 8080:8888 pythonserver
dfd29e6e06ec15533aec6d37b94ad23405ecf98ed0aa32954d3a5734ae252517
```

ici, on peut constater que le dossier test s'est bien créé.

![si cette image ne s'affiche pas, le fichier est présent dans le meme dossier que le markdown au nom de "volume.png"](./volume.png?raw=true)

## 3. Manipulation du démon docker

### 🌞 Modifier la configuration du démon Docker

trouvez le path du socket UNIX utilisé par défaut (c'est un fichier docker.sock)

```bash
arthur@ubuntu:/run$ sudo find / -name docker.sock -print 2>>/dev/null
/run/docker.sock
```

utiliser un socket TCP (port TCP) à la place

```bash
arthur@ubuntu:/run$ sudo dockerd -H tcp://0.0.0.0:2375
INFO[2021-09-19T03:50:45.068887807-07:00] Starting up                                  
WARN[2021-09-19T03:50:45.069263683-07:00] Binding to IP address without --tlsverify is insecure and gives root access on this machine to everyone who has access to your network.  host="tcp://0.0.0.0:2375"
[...]                  
INFO[2021-09-19T03:51:01.654255486-07:00] Docker daemon                                 commit=75249d8 graphdriver(s)=overlay2 version=20.10.8
INFO[2021-09-19T03:51:01.655573652-07:00] Daemon has completed initialization          
INFO[2021-09-19T03:51:01.689201549-07:00] API listen on [::]:2375
```

prouver que ça fonctionne en manipulant le démon Docker à travers le réseau (depuis une autre machine)

```bash
arthur@clone:~$ sudo docker -H 192.168.1.129:2375 ps
[sudo] password for arthur: 
CONTAINER ID   IMAGE     COMMAND        CREATED         STATUS         PORTS     NAMES
75b22cd5bb7d   alpine    "sleep 9999"   4 minutes ago   Up 4 minutes             boring_lamport
```

modifier l'emplacement des données Docker
trouver l'emplacement par défaut (c'est le "data-root")

```bash
arthur@ubuntu:~$ docker info | grep "Docker Root Dir"
 Docker Root Dir: /var/lib/docker
```

le déplacer dans un répertoire /data/docker que vous créerez à cet effet

```bash
arthur@ubuntu:~$ sudo mkdir -p /data/docker/
arthur@ubuntu:~$ cat /etc/docker/daemon.json 
{
  "data-root": "/data/docker"
}
```

```bash
# je redémarre mon service et je vérifie que les infos soient bien à jour
arthur@ubuntu:~$ sudo systemctl restart docker
arthur@ubuntu:~$ docker info | grep data
 Docker Root Dir: /data/docker
```

modifier le OOM score du démon Docker

renseignez-vous sur l'internet si vous ne savez pas à quoi ça correspond :)
expliquer succintement la valeur choisie
